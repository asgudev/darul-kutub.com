<?php

namespace SC\BooksBundle\Repositories;

use Doctrine\ORM\EntityRepository;
use SC\BooksBundle\Entity\BookAuthor;

class BookAuthorRepository extends EntityRepository
{

    /**
     * @param $name
     * @return BookAuthor[]
     */
    public function searchByName($name)
    {
        return $this->createQueryBuilder('b')
            ->where('b.name LIKE :name')
            ->setParameters([
                'name' => '%'.$name.'%',
            ])
            ->getQuery()
            ->getResult();
    }
}
