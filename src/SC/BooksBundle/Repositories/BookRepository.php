<?php

namespace SC\BooksBundle\Repositories;

use Doctrine\ORM\EntityRepository;
use SC\BooksBundle\Entity\Book;

class BookRepository extends EntityRepository
{

    public function findBadTomes()
    {
        return $this->createQueryBuilder("b")
            ->addSelect('bf')
            ->innerJoin("b.bookFile", 'bf')
            ->where('bf.bookPDF is null')
            ->andWhere('bf.bookFB2 is null')
            ->andWhere('bf.bookEPUB is null')
            ->getQuery()
            ->getResult();
    }

    public function findBySecondaryCategories(array $categories)
    {
        return $this->createQueryBuilder('b')
            ->join('b.secondary_categories', 'p')
            ->where('p.id in (:secondary_categories) and b.isPublished = 1')
            ->setParameter('secondary_categories', $categories)
            ->getQuery()
            ->getResult();
    }

    public function findByFiles(array $files)
    {
        return $this->createQueryBuilder('b')
            ->join('b.files', 'p')
            ->where('p.id in (:files) and b.isPublished = 1')
            ->setParameter('files', $files)
            ->getQuery()
            ->getResult();
    }


    public function searchByLanguage($language)
    {
        return $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->where('b.lang LIKE :language')
            ->leftJoin('b.bookAuthor', 'ba')
            ->setParameters([
                'language' => '%' . $language . '%'
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $title
     * @return Book[]
     */
    public function searchByTitle($title)
    {
        return $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->where('b.title LIKE :title')
            ->andWhere('b.isPublished = 1')
            ->leftJoin('b.bookAuthor', 'ba')
            ->setParameters([
                'title' => '%' . $title . '%'
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $title
     * @return Book[]
     */
    public function searchByAuthorAndTitle($author, $title)
    {
        return $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->where('b.title LIKE :title')
            ->andWhere('b.isPublished = 1')
            ->andWhere("ba.name LIKE :author")
            ->leftJoin('b.bookAuthor', 'ba')
            ->setParameters([
                'title' => '%' . $title . '%',
                'author' => '%' . $author . '%',

            ])
            ->getQuery()
            ->getResult();
    }

    public function findBookById($id)
    {
        return $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->addSelect('bf')
            ->where('b.id = :id')
            ->leftJoin('b.bookAuthor', 'ba')
            ->leftJoin('b.bookFile', 'bf')
            ->setParameters([
                'id' => $id
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return Book[]
     */
    public function findWithCategories()
    {
        return $this->createQueryBuilder('b')
            ->addSelect('c')
            ->leftJoin('b.category', 'c')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Book[]
     */
    public function findAllBooks()
    {
        return $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->leftJoin('b.bookAuthor', 'ba')
            ->orderBy('b.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Book[]
     */
    public function findAllBooksByCat($catId)
    {
        return $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->leftJoin('b.bookAuthor', 'ba')
            ->orderBy('b.createdAt', 'DESC')
            ->leftJoin('b.category', 'c')
            ->where('c.id = :catId')
            ->andWhere('b.isPublished = true')
            ->setParameters([
                'catId' => $catId
            ])
            ->getQuery()
            ->getResult();
    }


    /**
     * @param null $title
     * @param null $author
     * @param null $categoryId
     * @return mixed
     */
    public function findBook($title = null, $author = null, $categoryId = null, $primaryCatId = 11, $order = "ASC", $returnDql = false)
    {
        $books = $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->addSelect('bc')
            ->leftJoin('b.bookAuthor', 'ba')
            ->leftJoin('b.category', 'bc')
            ->where('b.isPublished = true')
            ->leftJoin('b.category', 'bc2')
            ->andWhere('b.primaryCategory = :bc2id')
            ->setParameters([
                'bc2id' => $primaryCatId,
            ]);


        if ($title) {
            $books
                ->andWhere('b.title LIKE :title')
                ->setParameter('title', '%' . $title . '%');
        }

        if ($author) {
            $books
                ->andWhere('ba.name LIKE :author')
                ->setParameter('author', '%' . $author . '%');
        }

        if ($categoryId) {
            $books
                ->andWhere('bc.id = :categoryId')
                ->setParameter('categoryId', $categoryId);
        }

        $books
            ->addOrderBy('b.id', $order);

        if ($returnDql) {
            return $books->getQuery();
        } else {
            return $books->getQuery()->getResult();
        }
    }

    public function findNewBooks($catId, $limit = 10, $returnDql = false)
    {
        $books = $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->leftJoin('b.bookAuthor', 'ba')
            ->leftJoin('b.category', 'c')
            ->where('b.isPublished = true')
            ->andWhere('b.primaryCategory = :catId')
            ->setParameters([
                'catId' => $catId,
            ])
            ->groupBy('b.id')
            ->orderBy('b.createdAt', 'DESC')
            ->setMaxResults($limit);

        if ($returnDql) {
            return $books->getQuery();
        } else {
            return $books->getQuery()->getResult();
        }


    }

    /**
     * @param null $query
     * @return mixed
     */
    public function findBookCombined($query, $returnDql = false)
    {
        $books = $this->createQueryBuilder('b')
            ->addSelect('ba')
            ->leftJoin('b.bookAuthor', 'ba')
            ->where('b.isPublished = true')
            ->andWhere('CONCAT(b.title, \' \', ba.name) LIKE :query')
            ->setParameters([
                'query' => '%' . $query . '%'
            ]);

        if ($returnDql) {
            return $books->getQuery();
        } else {
            return $books->getQuery()->getResult();
        }


    }


    public function getBookCount($primaryCategoryId = 11)
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b)')
            ->where('b.isPublished = true')
            ->andWhere('b.primaryCategory = :pcid')
            ->setParameters([
                'pcid' => $primaryCategoryId
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }
}

