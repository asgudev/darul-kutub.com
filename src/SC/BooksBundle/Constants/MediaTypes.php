<?php

namespace SC\BooksBundle\Constants;

class MediaTypes
{
    protected static $types = array(
        1 => 'pdf'
    );

    public static function getTypeIdByExt($ext)
    {
        return array_search($ext, self::$types);
    }
}