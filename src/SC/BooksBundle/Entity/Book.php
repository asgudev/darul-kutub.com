<?php

namespace SC\BooksBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use SC\CategoryBundle\Entity\Category;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="sc_books")
 * @ORM\Entity(repositoryClass="SC\BooksBundle\Repositories\BookRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="SC\BooksBundle\Entity\BookAuthor", mappedBy="books")
     */
    private $bookAuthor;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $title;


    /**
     * Основная категория
     *
     * @ORM\ManyToOne(targetEntity="SC\CategoryBundle\Entity\Category")
     */
    private $primaryCategory;

    /**
     * Основная категория
     *
     * @ORM\ManyToMany(targetEntity="SC\CategoryBundle\Entity\Category")
     */
    private $category;

    /**
     * @ORM\Column(type="string", name="lang", nullable=true)
     */
    private $lang;


    /**
     * @ORM\OneToMany(targetEntity="SC\BooksBundle\Entity\BookFile", mappedBy="book", cascade={"persist"})
     */
    private $bookFile;

    /**
     * @ORM\Column(type="text", name="annotation", nullable=true)
     */
    private $annotation;


    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $pages;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $publishingHouse;


    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    private $isPublished;

    /**
     * @ORM\Column(type="boolean", name="is_recommended")
     */
    private $isRecommended;


    /**
     * @ORM\Column(type="string", name="slug", nullable=true)
     */
    private $slug;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bookFile = new ArrayCollection();

        $this->createdAt = new \DateTime();
        $this->isPublished = 0;
        $this->isRecommended = 0;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Book
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @ORM\Column(type="string", name="cover", length=255, nullable=true)
     */
    public $cover;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $coverFile;

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setCoverFile(UploadedFile $file = null)
    {
        $this->coverFile = $file;
        // check if we have an old cover path
        if (isset($this->cover)) {
            // store the old name to delete after the update
            $this->temp = $this->cover;
            $this->cover = null;
        } else {
            $this->cover = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getCoverFile()
    {
        return $this->coverFile;
    }

    public function getCoverAbsolutePath()
    {
        return null === $this->cover
            ? null
            : $this->getCoverUploadRootDir() . '/' . $this->cover;
    }

    public function getCover()
    {
        return null === $this->cover
            ? $this->getCoverUploadDir() . '/placeholder.jpg'
            : $this->getCoverUploadDir() . '/' . $this->cover;
    }

    public function getCoverPath()
    {
        return null === $this->cover
            ? $this->getCoverUploadDir() . '/placeholder.jpg'
            : $this->getCoverUploadDir() . '/' . $this->cover;
    }

    protected function getCoverUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getCoverUploadDir();
    }

    protected function getCoverUploadDir()
    {
        return 'uploads/covers';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preCoverUpload()
    {
        if (null !== $this->getCoverFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->cover = $filename . '.' . $this->getCoverFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadCover()
    {
        if (null === $this->getCoverFile()) {
            return;
        }
        $this->getCoverFile()->move($this->getCoverUploadRootDir(), $this->cover);

        // check if we have an old cover
        if (isset($this->temp)) {
            // delete the old cover
            unlink($this->getCoverUploadRootDir() . '/' . $this->temp);
            // clear the temp cover path
            $this->temp = null;
        }
        $this->coverFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeCoverUpload()
    {
        $file = $this->getCoverAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }


    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Set annotation
     *
     * @param string $annotation
     *
     * @return Book
     */
    public function setAnnotation($annotation)
    {
        $this->annotation = $annotation;

        return $this;
    }

    /**
     * Get annotation
     *
     * @return string
     */
    public function getAnnotation()
    {
        return $this->annotation;
    }

    /**
     * Set pages
     *
     * @param string $pages
     *
     * @return Book
     */
    public function setPages($pages)
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * Get pages
     *
     * @return string
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return Book
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set publishingHouse
     *
     * @param string $publishingHouse
     *
     * @return Book
     */
    public function setPublishingHouse($publishingHouse)
    {
        $this->publishingHouse = $publishingHouse;

        return $this;
    }

    /**
     * Get publishingHouse
     *
     * @return string
     */
    public function getPublishingHouse()
    {
        return $this->publishingHouse;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     *
     * @return Book
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set isRecommended
     *
     * @param boolean $isRecommended
     *
     * @return Book
     */
    public function setIsRecommended($isRecommended)
    {
        $this->isRecommended = $isRecommended;

        return $this;
    }

    /**
     * Get isRecommended
     *
     * @return boolean
     */
    public function getIsRecommended()
    {
        return $this->isRecommended;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Book
     */
    public function setPrimaryCategory(Category $category = null)
    {
        $this->primaryCategory = $category;

        return $this;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Book
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    public function addCategory(Category $category = null)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getPrimaryCategory()
    {
        return $this->primaryCategory;
    }

    /**
     * Set lang
     *
     * @param string $lang
     *
     * @return Book
     */
    public function setLang($lang = null)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Add bookFile
     *
     * @param \SC\BooksBundle\Entity\BookFile $bookFile
     *
     * @return Book
     */
    public function addBookFile(\SC\BooksBundle\Entity\BookFile $bookFile)
    {
        $this->bookFile[] = $bookFile;
        $bookFile->setBook($this);

        return $this;
    }

    /**
     * Remove bookFile
     *
     * @param \SC\BooksBundle\Entity\BookFile $bookFile
     */
    public function removeBookFile(\SC\BooksBundle\Entity\BookFile $bookFile)
    {
        $this->bookFile->removeElement($bookFile);
        $bookFile->setBook(null);
    }


    /**
     * Get bookFile
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookFile()
    {
        return $this->bookFile;
    }



    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Book
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add bookAuthor
     *
     * @param \SC\BooksBundle\Entity\BookAuthor $bookAuthor
     *
     * @return Book
     */
    public function addBookAuthor(\SC\BooksBundle\Entity\BookAuthor $bookAuthor)
    {
        $this->bookAuthor[] = $bookAuthor;
        $bookAuthor->addBook($this);

        return $this;
    }

    /**
     * Remove bookAuthor
     *
     * @param \SC\BooksBundle\Entity\BookAuthor $bookAuthor
     */
    public function removeBookAuthor(\SC\BooksBundle\Entity\BookAuthor $bookAuthor)
    {
        $this->bookAuthor->removeElement($bookAuthor);
        $bookAuthor->removeBook($this);

    }

    /**
     * Get bookAuthor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookAuthor()
    {
        return $this->bookAuthor;
    }
}
