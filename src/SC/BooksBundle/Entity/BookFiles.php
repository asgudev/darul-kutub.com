<?php

namespace SC\BooksBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_books_files")
 */
class BookFiles
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $filename;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="integer")
     */
    protected $mediaTypeId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $size;

    /**
     * @ORM\Column(type="integer")
     */
    protected $downloadCnt = 0;

    protected $fileUrl;


    /**
     * @param mixed $fileUrl
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;
    }

    /**
     * @return mixed
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return BookFiles
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BookFiles
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set mediaTypeId
     *
     * @param integer $mediaTypeId
     * @return BookFiles
     */
    public function setMediaTypeId($mediaTypeId)
    {
        $this->mediaTypeId = $mediaTypeId;
    
        return $this;
    }

    /**
     * Get mediaTypeId
     *
     * @return integer 
     */
    public function getMediaTypeId()
    {
        return $this->mediaTypeId;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return BookFiles
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set downloadCnt
     *
     * @param integer $downloadCnt
     * @return BookFiles
     */
    public function setDownloadCnt($downloadCnt)
    {
        $this->downloadCnt = $downloadCnt;
    
        return $this;
    }

    /**
     * Get downloadCnt
     *
     * @return integer 
     */
    public function getDownloadCnt()
    {
        return $this->downloadCnt;
    }
}
