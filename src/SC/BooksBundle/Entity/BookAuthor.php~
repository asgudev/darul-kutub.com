<?php

namespace SC\BooksBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_books_author")
 */
class BookAuthor
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $name = '';


    /**
     * @ORM\Column(type="integer")
     */
    protected $letter = '';


    /**
     * @ORM\Column(type="text")
     */
    protected $bio = '';


    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity="Book", mappedBy="authors")
     */
    protected $books;

    /**
     * @ORM\OneToOne(targetEntity="BookAuthorAva", cascade={"persist", "remove"})
     */
    protected $ava;


    protected $avaUrl;

    /**
     * @param mixed $avaUrl
     */
    public function setAvaUrl($avaUrl)
    {
        $this->avaUrl = $avaUrl;
    }

    /**
     * @return mixed
     */
    public function getAvaUrl()
    {
        return $this->avaUrl;
    }

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BookAuthor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set bio
     *
     * @param string $bio
     * @return BookAuthor
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BookAuthor
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add books
     *
     * @param \SC\BooksBundle\Entity\Book $books
     * @return BookAuthor
     */
    public function addBook(\SC\BooksBundle\Entity\Book $books)
    {
        $this->books[] = $books;

        return $this;
    }

    /**
     * Remove books
     *
     * @param \SC\BooksBundle\Entity\Book $books
     */
    public function removeBook(\SC\BooksBundle\Entity\Book $books)
    {
        $this->books->removeElement($books);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * Set letter
     *
     * @return BookAuthor
     */
    public function setLetter()
    {
        $this->letter = $this->ordutf8(mb_strtolower(mb_substr($this->name, 0, 1, 'utf8'), 'utf8'));

        return $this;
    }

    /**
     * Get letter
     *
     * @return integer
     */
    public function getLetter()
    {
        return $this->letter;
    }

    protected function ordutf8($string, $offset = 0)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) $offset = -1;
        return $code;
    }

    /**
     * Set ava
     *
     * @param \SC\BooksBundle\Entity\BookAuthorAva $ava
     * @return BookAuthor
     */
    public function setAva(\SC\BooksBundle\Entity\BookAuthorAva $ava = null)
    {
        $this->ava = $ava;
    
        return $this;
    }

    /**
     * Get ava
     *
     * @return \SC\BooksBundle\Entity\BookAuthorAva 
     */
    public function getAva()
    {
        return $this->ava;
    }
}