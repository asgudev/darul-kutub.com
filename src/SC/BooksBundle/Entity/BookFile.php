<?php

namespace SC\BooksBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_books_file")
 * @ORM\HasLifecycleCallbacks
 *
 */
class BookFile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="SC\BooksBundle\Entity\Book", inversedBy="bookFile")
     */
    private $book;

    /**
     * @ORM\Column(name="book_file_name", type="string", nullable=true)
     */
    private $bookFileName;

    /**
     * @ORM\Column(type="string", name="book_pdf",  length=250, nullable=true)
     */
    private $bookPDF;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $bookPDFFile;

    /**
     * @ORM\Column(type="string", name="book_epub",  length=250, nullable=true)
     */
    private $bookEPUB;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $bookEPUBFile;

    /**
     * @ORM\Column(type="string", name="book_fb2",  length=250, nullable=true)
     */
    private $bookFB2;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $bookFB2File;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $size;

    /**
     * @ORM\Column(type="integer")
     */
    private $downloadCnt;

    

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->downloadCnt = 0;
    }


    public function getBookPDFPath(){
        return null === $this->bookPDF
            ? $this->getBookUploadDir() . '/placeholder.jpg'
            : $this->getBookUploadDir() . '/' . $this->bookPDF;
    }

    public function getBookFB2Path(){
        return null === $this->bookFB2
            ? $this->getBookUploadDir() . '/placeholder.jpg'
            : $this->getBookUploadDir() . '/' . $this->bookFB2;
    }

    public function getBookEPUBPath(){
        return null === $this->bookEPUB
            ? $this->getBookUploadDir() . '/placeholder.jpg'
            : $this->getBookUploadDir() . '/' . $this->bookEPUB;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookFileName
     *
     * @param string $bookFileName
     *
     * @return BookFile
     */
    public function setBookFileName($bookFileName)
    {
        $this->bookFileName = $bookFileName;

        return $this;
    }

    /**
     * Get bookFileName
     *
     * @return string
     */
    public function getBookFileName()
    {
        return $this->bookFileName;
    }

    /**
     * Set bookPDF
     *
     * @param string $bookPDF
     *
     * @return BookFile
     */
    public function setBookPDF($bookPDF)
    {
        $this->bookPDF = $bookPDF;

        return $this;
    }

    /**
     * Get bookPDF
     *
     * @return string
     */
    public function getBookPDF()
    {
        return $this->bookPDF;
    }

    /**
     * Set bookEPUB
     *
     * @param string $bookEPUB
     *
     * @return BookFile
     */
    public function setBookEPUB($bookEPUB)
    {
        $this->bookEPUB = $bookEPUB;

        return $this;
    }

    /**
     * Get bookEPUB
     *
     * @return string
     */
    public function getBookEPUB()
    {
        return $this->bookEPUB;
    }

    /**
     * Set bookFB2
     *
     * @param string $bookFB2
     *
     * @return BookFile
     */
    public function setBookFB2($bookFB2)
    {
        $this->bookFB2 = $bookFB2;

        return $this;
    }

    /**
     * Get bookFB2
     *
     * @return string
     */
    public function getBookFB2()
    {
        return $this->bookFB2;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BookFile
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return BookFile
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set downloadCnt
     *
     * @param integer $downloadCnt
     *
     * @return BookFile
     */
    public function setDownloadCnt($downloadCnt)
    {
        $this->downloadCnt = $downloadCnt;

        return $this;
    }

    /**
     * Get downloadCnt
     *
     * @return integer
     */
    public function getDownloadCnt()
    {
        return $this->downloadCnt;
    }

    /**
     * Set book
     *
     * @param \SC\BooksBundle\Entity\Book $book
     *
     * @return BookFile
     */
    public function setBook(\SC\BooksBundle\Entity\Book $book = null)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return \SC\BooksBundle\Entity\Book
     */
    public function getBook()
    {
        return $this->book;
    }



    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setBookPDFFile(UploadedFile $file = null)
    {
        $this->bookPDFFile = $file;
        // check if we have an old book path
        if (isset($this->bookPDF)) {
            // store the old name to delete after the update
            $this->tempPDF = $this->bookPDF;
            $this->bookPDF = null;
        } else {
            $this->bookPDF = 'initial';
        }
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setBookFB2File(UploadedFile $file = null)
    {
        $this->bookFB2File = $file;
        // check if we have an old book path
        if (isset($this->bookFB2)) {
            // store the old name to delete after the update
            $this->tempFB2 = $this->bookFB2;
            $this->bookFB2 = null;
        } else {
            $this->bookFB2 = 'initial';
        }
    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setBookEPUBFile(UploadedFile $file = null)
    {
        $this->bookEPUBFile = $file;
        // check if we have an old book path
        if (isset($this->bookEPUB)) {
            // store the old name to delete after the update
            $this->tempEPUB = $this->bookEPUB;
            $this->bookEPUB = null;
        } else {
            $this->bookEPUB = 'initial';
        }
    }



    private $tempPDF;
    private $tempFB2;
    private $tempEPUB;

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getBookPDFFile()
    {
        return $this->bookPDFFile;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getBookFB2File()
    {
        return $this->bookFB2File;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getBookEPUBFile()
    {
        return $this->bookEPUBFile;
    }

    public function getBookAbsolutePath()
    {
        return null === $this->book
            ? null
            : $this->getBookUploadRootDir() . '/' . $this->book;
    }

    public function getBookPath()
    {
        return null === $this->book
            ? $this->getBookUploadDir() . '/placeholder.jpg'
            : $this->getBookUploadDir() . '/' . $this->book;
    }

    protected function getBookUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getBookUploadDir();
    }

    protected function getBookUploadDir()
    {
        return 'uploads/books';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preBookPDFUpload()
    {
        if (null !== $this->getBookPDFFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->bookPDF = $filename . '.' . $this->getBookPDFFile()->guessExtension();
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preBookFB2Upload()
    {
        if (null !== $this->getBookFB2File()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->bookFB2 = $filename . '.' . $this->getBookFB2File()->guessExtension();
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preBookEPUBUpload()
    {
        if (null !== $this->getBookEPUBFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->bookEPUB = $filename . '.' . $this->getBookEPUBFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadBookPDF()
    {
        if (null === $this->getBookPDFFile()) {
            return;
        }
        $this->getBookPDFFile()->move($this->getBookUploadRootDir(), $this->bookPDF);

        // check if we have an old book
        if (isset($this->tempPDF)) {
            // delete the old book
            unlink($this->getBookUploadRootDir() . '/' . $this->tempPDF);
            // clear the temp book path
            $this->tempPDF = null;
        }
        $this->bookPDFFile = null;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadBookFB2()
    {
        if (null === $this->getBookFB2File()) {
            return;
        }
        $this->getBookFB2File()->move($this->getBookUploadRootDir(), $this->bookFB2);

        // check if we have an old book
        if (isset($this->tempFB2)) {
            // delete the old book
            unlink($this->getBookUploadRootDir() . '/' . $this->tempFB2);
            // clear the temp book path
            $this->tempFB2 = null;
        }
        $this->bookFB2File = null;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadBookEPUB()
    {
        if (null === $this->getBookEPUBFile()) {
            return;
        }
        $this->getBookEPUBFile()->move($this->getBookUploadRootDir(), $this->bookEPUB);

        // check if we have an old book
        if (isset($this->tempEPUB)) {
            // delete the old book
            unlink($this->getBookUploadRootDir() . '/' . $this->tempEPUB);
            // clear the temp book path
            $this->tempEPUB = null;
        }
        $this->bookEPUBFile = null;
    }



    /**
     * @ORM\PostRemove()
     */
    public function removeBookPDFUpload()
    {
        $file = $this->getBookAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }
}
