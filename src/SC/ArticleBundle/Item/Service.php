<?php

namespace SC\ArticleBundle\Item;


use SC\ArticleBundle\Entity\Article;
use SC\ArticleBundle\Entity\ArticleFlatContent;

class Service
{
    protected $fileStorageService;
    protected $entityManager;
    protected $container;

    public function __construct($fileStorageService, $entityManager, $container)
    {
        $this->fileStorageService = $fileStorageService;
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    public static function getFileStorageClassName()
    {
        /*
         * TODO: брать из конфига
         */
        return '\SC\ArticleBundle\FS\Module';
    }

    public function hydrateAuthors(array $items)
    {
        $ids = array();

        foreach ($items as $item) {
            $ids[$item->getAuthorId()] = $item->getAuthorId();
        }

        if (empty($ids)) {
            return $items;
        }

        $authors = $this->entityManager->getRepository('SCUserBundle:BaseUser')->findById($ids);

        foreach ($items as $obj) {
            foreach ($authors as $author) {
                if ($obj->getAuthorId() == $author->getId()) {
                    $obj->setAuthor($author);
                }
            }
        }

        return $items;
    }


    public function hydratePreviews(array $items)
    {
        $fileStorageClassName = self::getFileStorageClassName();

        $ids = array();

        foreach ($items as $item) {
            $ids[$item->getId()] = $item->getId();
        }

        if (empty($ids)) {
            return $items;
        }

        $files = $this->fileStorageService->getFilesFor($fileStorageClassName::getFileStorageModuleId(),
            $ids, $fileStorageClassName::FILE_TYPE_PREVIEW);


        foreach ($items as $obj) {
            $_prevArray = array();
            foreach ($files as $__fid => $file) {
                if ($file->getItemId() == $obj->getId()) {
                    $_prevArray[$files[$__fid]->getModSubTypeId()] = $files[$__fid];
                }
            }
            $obj->setPreviews($_prevArray);

        }

        return $items;
    }

    public function hydrateFiles(array $items)
    {
        $fileStorageClassName = self::getFileStorageClassName();

        $ids = array();

        foreach ($items as $item) {
            $ids[$item->getId()] = $item->getId();
        }

        if (empty($ids)) {
            return $items;
        }


        $files = $this->fileStorageService->getFilesFor($fileStorageClassName::getFileStorageModuleId(), $ids);
        foreach ($items as $obj) {
            $__files = array();
            foreach ($files as $__fid => $file) {
                if ($file->getItemId() == $obj->getId()) {
                    $__files[$file->getId()] = $file;
                }
            }
            $obj->setFiles($__files);

        }
        return $items;
    }


    public function hydrateFlatContent (array $items)
    {
        $ids = array();
        foreach ($items as $item) {
            $ids[$item->getId()] = $item->getId();
        }

        if (empty($ids)) {
            return $items;
        }

        $flatContents = $this->entityManager->getRepository('SCArticleBundle:ArticleFlatContent')->findByArticle($ids);

        foreach ($items as $id => $itemObj) {
            foreach ($flatContents as $__flatcont) {
                if ($__flatcont->getArticle()->getId() == $itemObj->getId()) {
                    $itemObj->setFlatContent($__flatcont);
                }
            }
        }

        return $items;
    }

    public function hydrateContent (array $items)
    {
        $ids = array();
        foreach ($items as $item) {
            $ids[$item->getId()] = $item->getId();
        }

        if (empty($ids)) {
            return $items;
        }

        $contents = $this->entityManager->getRepository('SCArticleBundle:ArticleContent')->findByArticle($ids);

        foreach ($items as $itemObj) {
            foreach ($contents as $__cont) {
                if ($__cont->getArticle()->getId() == $itemObj->getId()) {
                    $itemObj->setContent($__cont);
                }
            }
        }

        return $items;
    }

    public function generateFlatContent(Article $item)
    {
        /*
         * TODO: унести в конфиги
         */
        $integrators = array('sc_article_item_integrator.filestorage');

        $renderedContent = $item->getContent()->getTypographedContent();

        foreach ($integrators as $intClassName) {
            /**
             * ВАЖНО! В задачи интегратора так же входит создание и поддержка связей между элементами (между статьями и файлами статей или между статьями и элементами энциклопедии)
             */
            $renderedContent = $this->container->get($intClassName)->setText($renderedContent)->setArticle($item)->getRenderedContent();
        }

        $flatContent = $this->entityManager->getRepository('SCArticleBundle:ArticleFlatContent')->findOneByArticle($item);

        if (!is_null($flatContent)) {
            $flatContent->setFlatContent($renderedContent);
            $this->entityManager->persist($flatContent);
            $this->entityManager->flush();
            $item->setFlatContent($flatContent);
        }

        

    }

}