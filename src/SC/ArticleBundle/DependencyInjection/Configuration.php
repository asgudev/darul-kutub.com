<?php

namespace SC\ArticleBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sc_article');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $this->addIndexesSection($rootNode);
        $this->addHostnameSection($rootNode);
        $this->addPreviewSizesSection($rootNode);

        return $treeBuilder;
    }


    private function addIndexesSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('indexes')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('main')->defaultNull()->end()
            ->scalarNode('delta')->defaultNull()->end()
            ->end()
            ->end()
            ->end();
    }

    private function addHostnameSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('hostname')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('url')->defaultNull()->end()
            ->end()
            ->end()
            ->end();
    }

    private function addPreviewSizesSection(ArrayNodeDefinition $node) {
        $node
            ->children()
            ->arrayNode('sizes')->addDefaultsIfNotSet()
                ->children()
                    ->arrayNode('big')->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('width')->defaultValue(320)->end()
                            ->scalarNode('height')->defaultValue(260)->end()
                        ->end()
                    ->end()
                    ->arrayNode('medium')->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('width')->defaultValue(250)->end()
                            ->scalarNode('height')->defaultValue(203)->end()
                        ->end()
                    ->end()
                    ->arrayNode('small')->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('width')->defaultValue(190)->end()
                            ->scalarNode('height')->defaultValue(130)->end()
                        ->end()
                    ->end()
                ->end()
        ->end();
    }
}
