<?php

namespace SC\ArticleBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SCArticleExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');


        if( isset($config['indexes']) ) {
            $container->setParameter('sc_article.indexes.main', $config['indexes']['main']);
            $container->setParameter('sc_article.indexes.delta', $config['indexes']['delta']);
        }

        if( isset($config['hostname']) ) {
            $container->setParameter('sc_article.hostname.url', $config['hostname']['url']);
        }

        if( isset($config['sizes']) ) {
            $container->setParameter('sc_article.sizes', $config['sizes']);
        }

    }


}
