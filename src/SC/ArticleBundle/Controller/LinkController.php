<?php

namespace SC\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SC\ArticleBundle\Entity\Article;

/*
 * TODO: переделать на хелпер к твигу
 */
class LinkController extends Controller
{
    public function getAction(Article $article)
    {
        $link = '/' . self::createImpresionUrlSlug($article->getPrimaryCategory()->getPath(), preg_replace("#\*\*(.*?)\*\*#", "\\1", $article->getTitle()), $article->getId());
        return $this->render('SCArticleBundle:Link:get.html.twig', array('link' => $link));
    }

    public static function getUrl(Article $article)
    {
        return '/' . self::createImpresionUrlSlug($article->getPrimaryCategory()->getPath(), preg_replace("#\*\*(.*?)\*\*#", "\\1", $article->getTitle()), $article->getId());
    }

    public static function createImpresionUrlSlug($category, $title = '', $id)
    {
        $title = self::createSlug($title);

        if (strlen($title) < 3) {
            // для случаев когда у нас ни тайтла ни агентства нет в заголовке остается '-'
            $title = 'notitle';
        }

        return preg_replace("/-+/", "-", $category . '/' . $title . '-' . $id . '/');
    }

    static public function createSlug($slug_string)
    {
        $slug_string = trim($slug_string);

        $t = \Transliterator::create("latin; NFKD; [^\u0000-\u007E] Remove; NFC");
        $slug_string = mb_strtolower($slug_string, 'utf8');
        $slug_string = $t->transliterate($slug_string);

//        $slug_string = transliterate($slug_string, array(
//            "cyrillic_lowercase",
//            "latin_lowercase",
//            'han_transliterate',
//            'diacritical_remove',
//            'cyrillic_transliterate'
//        ), 'utf-8', 'utf-8');

        $slug_string = strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $slug_string));

        $code_entities_match = array(
            ' ',
            '--',
            '&quot;',
            '!',
            '@',
            '#',
            '$',
            '%',
            '^',
            '&',
            '*',
            '(',
            ')',
            '_',
            '+',
            '{',
            '}',
            '|',
            ':',
            '"',
            '<',
            '>',
            '?',
            '[',
            ']',
            '\\',
            ';',
            "'",
            ',',
            '.',
            '/',
            '*',
            '+',
            '~',
            '`',
            '='
        );
        $code_entities_replace = array(
            '-',
            '-',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $slug_string = str_replace($code_entities_match, $code_entities_replace, $slug_string);
        $slug_string = preg_replace("/-+/", "-", $slug_string);

        return $slug_string;
    }
}
