<?php
namespace SC\ArticleBundle\Preview;
class Types
{
    const SQUARE = 70;
    const RECT_SMALL = 190;
    const RECT_MEDIUM = 200;
    const RECT_BIG = 440;
    const UNKNOWN = 1;

    public static $sizes = array(
        'rect' => array(
            self::RECT_BIG => array(
                0 => 440,
                1 => 260
            ),
            self::RECT_MEDIUM => array(
                0 => 200,
                1 => 140
            ),
            self::RECT_SMALL => array(
                0 => 190,
                1 => 130
            )
        ),
        'square' => array(
            self::SQUARE => array(
                0 => 70,
                1 => 70
            )
        )
    );
}