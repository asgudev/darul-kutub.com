<?php

namespace SC\FileStorageBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_file_article")
 */
class File
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    protected $moduleId; // это поле не хранится в таблице

    /**
     * @ORM\Column(type="integer")
     */
    protected $itemId;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $fileName;

    /**
     * @ORM\Column(type="integer")
     */
    protected $originHostId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $modTypeId = 0;

    /**
     * @ORM\Column(type="integer")
     */
    protected $modSubTypeId = 0;

    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    protected $isDeleted = 0;

    /**
     * @ORM\Column(type="integer")
     */
    protected $size;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="integer")
     */
    protected $mediaTypeId = \SC\FileStorageBundle\Service\MediaTypes::UNKNOWN;

    /**
     * @ORM\Column(type="text")
     */
    protected $mediaData = array(); // тут храним инфу о медиатипе типа width, height и все такое



    protected $url;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     * @return File
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return File
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set originHostId
     *
     * @param integer $originHostId
     * @return File
     */
    public function setOriginHostId($originHostId)
    {
        $this->originHostId = $originHostId;

        return $this;
    }

    /**
     * Get originHostId
     *
     * @return integer
     */
    public function getOriginHostId()
    {
        return $this->originHostId;
    }

    /**
     * Set modTypeId
     *
     * @param integer $modTypeId
     * @return File
     */
    public function setModTypeId($modTypeId)
    {
        $this->modTypeId = $modTypeId;

        return $this;
    }

    /**
     * Get modTypeId
     *
     * @return integer
     */
    public function getModTypeId()
    {
        return $this->modTypeId;
    }

    /**
     * Set modSubTypeId
     *
     * @param integer $modSubTypeId
     * @return File
     */
    public function setModSubTypeId($modSubTypeId)
    {
        $this->modSubTypeId = $modSubTypeId;

        return $this;
    }

    /**
     * Get modSubTypeId
     *
     * @return integer
     */
    public function getModSubTypeId()
    {
        return $this->modSubTypeId;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return File
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return File
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set mediaTypeId
     *
     * @param integer $mediaTypeId
     * @return File
     */
    public function setMediaTypeId($mediaTypeId)
    {
        $this->mediaTypeId = $mediaTypeId;

        return $this;
    }

    /**
     * Get mediaTypeId
     *
     * @return integer
     */
    public function getMediaTypeId()
    {
        return $this->mediaTypeId;
    }

    /**
     * Set mediaData
     *
     * @param string $mediaData
     * @return File
     */
    public function setMediaData($mediaData)
    {
        $this->mediaData = serialize($mediaData);

        return $this;
    }

    /**
     * Get mediaData
     *
     * @return string
     */
    public function getMediaData()
    {
        return unserialize($this->mediaData);
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getUrl ()
    {
        return $this->url;
    }

    public function getModuleId ()
    {
        return $this->moduleId;
    }

    public function setModuleId ($moduleId)
    {
        $this->moduleId = $moduleId;
    }

}