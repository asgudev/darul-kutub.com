<?php
namespace SC\FileStorageBundle\Module;

interface ModuleInterface
{
    public static function getFileStorageModuleId();

    public static function getHRModuleName();

    public static function getItemLocalFilePath($itemId);

    public static function getItemRelativeWebPath($itemId);

    public static function getTableId($itemId);

    public function getFileUrl(\SC\FileStorageBundle\Entity\File $file, $moduleSettings);
}