<?php

namespace SC\FileStorageBundle\Service;
/*
 * TODO: перенести из сервиса куда-нить в fileinfo
 */

class MediaTypes
{
    const UNKNOWN = 0;
    const IMAGE = 1;
    const VIDEO = 2;
    const AUDIO = 3;
    const PDF = 4;
    const MOBI = 5;
}
