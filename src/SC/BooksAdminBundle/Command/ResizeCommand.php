<?php
namespace SC\BooksAdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Class for AbstractCommand
 */
class ResizeCommand extends ContainerAwareCommand
{
    /**
     * Configure the command.
     */
    protected function configure()
    {
        $this
            ->setName('books:resize')
            ->setDescription('Resize new book cover');
    }

    /**
     * Executes the command.
     *
     * @param InputInterface $input  The input
     * @param OutputInterface $output The output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $books = $this->getContainer()->get('doctrine')->getRepository('SCBooksBundle:Book')->findAll();

        $covers = array();
        foreach ($books as $book) {
            $localUploadPath = '/home/web/origin.darul-kutub.com/web/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId();

            $coverFile = null;
            foreach (new \DirectoryIterator($localUploadPath) as $file) {
                if ($file->getExtension() == 'jpg' || $file->getExtension() == 'png') {
                    if (is_null($coverFile)) {
                        $coverFile = $file;
                    } else {
                        if ($coverFile->getMTime() <= $file->getMTime()) {
                            $coverFile = $file;
                        }
                    }

                    $covers[$book->getId()] = clone $coverFile;
                }
            }
        }

        $previewWidth = 120;
        $previewHeigth = 180;
        foreach ($covers as $bookId => $cover) {
            $localUploadPath = '/home/web/origin.darul-kutub.com/web/files/books/part_' . (floor($bookId / 10000)) . '/' . $bookId;
            $localCoverPath = $localUploadPath . '/cover';
            $localCoverOrigPath = $localCoverPath . '/orig';

            if (!file_exists($localCoverOrigPath)) {
                mkdir($localCoverOrigPath, 0777, true);
            }

            $newOrigPathname = $localCoverOrigPath . '/' . $bookId . '_orig.' . $cover->getExtension();
            exec('mv ' . $cover->getPathname() . ' ' . $newOrigPathname);

            $targetFileName = $localCoverPath . '/' . $bookId . '.jpg';

            $resampler = new \SC\MediaBundle\Image\Resampler();
            $resampler->setTargetQuality(90);
            $resampler->setSourceFileName($newOrigPathname);
            $resampler->setTargetHeight($previewHeigth);
            $resampler->setTargetWidth($previewWidth);

            $resampler->resample();

            $resampler->save($targetFileName, IMAGETYPE_JPEG);

        }
    }
}