<?php

namespace SC\BooksAdminBundle\Controller;

use SC\BooksBundle\Entity\BookAuthor;
use SC\BooksBundle\Entity\BookAuthorAva;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AuthorsController extends Controller
{
    public function indexAction(Request $request)
    {
        //$authors = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findBy(array(), array('createdAt' => 'DESC'));

        if ($request->get('q') == null) {
            $authors = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findBy(array(), array('createdAt' => 'DESC'));
        } else {
            $authors = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->searchByName($request->get('q'));
        }


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $authors,
            $request->query->get('page', 1),
            $limit = 20
        );


        $data = array();
        $data['q'] = '';
        $data['pagination'] = $pagination;

        return $this->render('SCBooksAdminBundle:Authors:index.html.twig', $data);
    }

    public function newAction()
    {
        $author = new BookAuthor();
        $author->setName('');
        $author->setBio('');
        $author->setCreatedAt(new \DateTime());

        $this->getDoctrine()->getManager()->persist($author);
        $this->getDoctrine()->getManager()->flush();
        return $this
            ->redirect(
                $this
                    ->generateUrl('sc_books_admin_authors_edit',
                        array('id' => $author->getId())));
    }

    public function editAction($id)
    {
        $author = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findOneById($id);

        if (is_null($author)) {
            return new Response('нет такого айди', 404);
        }

        $avaUrl = '/files/authors/part_' . (floor($author->getId() / 10000)) . '/' . $author->getId() . '/ava-' . $author->getId() . '.jpg';
        $author->setAvaUrl($avaUrl);

        return $this->render('SCBooksAdminBundle:Authors:edit.html.twig', array('author' => $author));

    }

    public function saveAction(Request $request)
    {
        $authorId = (int)$request->get('author_id');

        if ($authorId == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такого айди'));
        }

        $author = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findOneById($authorId);

        if (is_null($author)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такого айди'));
        }

        $fio = $request->get('author_fio');

        if (strlen($fio) == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет имени'));
        }

        $bio = $request->get('author_bio');

        $author->setName($fio);
        $author->setBio($bio);
        $author->setLetter();


        if (isset($_FILES['author_ava']) && $request->get('coords')['x'] != '' && $request->get('coords')['y'] != '') {
            $this->saveAva($_FILES['author_ava'], $author, $request->get('coords'));
        }

        $this->getDoctrine()->getManager()->persist($author);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('code' => 'saved'));
    }

    public function saveAva($file, $author, $coords)
    {
        //1. сохранить оригинал
        $targetFileName = $file['name'];
        $tmpFileName = $file['tmp_name'];

        // everything to lower and no spaces begin or end
        $targetFileName = strtolower(trim($targetFileName));

        // adding - for spaces and union characters
        $find = array(' ', '&', '\r\n', '\n', '+', ',');
        $targetFileName = str_replace($find, '-', $targetFileName);

        //delete and replace rest of special chars
        $find = array('/[^a-z0-9\-<>.]/', '/[\-]+/', '/<[^>]*>/');
        $repl = array('', '-', '');
        $targetFileName = preg_replace($find, $repl, $targetFileName);

        $localUploadPath = WEB_DIRECTORY . '/files/authors/part_' . (floor($author->getId() / 10000)) . '/' . $author->getId();
        $localOrigPath = $localUploadPath . '/orig';

        if (!file_exists($localUploadPath)) {
            mkdir($localUploadPath, 0777, true);
        }

        if (!file_exists($localOrigPath)) {
            mkdir($localOrigPath, 0777, true);
        }

        $fullFileName = $localOrigPath . '/' . $targetFileName;
        if (!is_null($author->getAva())) {
            $oldFilename = $localOrigPath . '/' . $author->getAva()->getFilename();

            if (!is_dir($oldFilename) && file_exists($oldFilename)) {
                unlink($oldFilename);

                foreach (glob($localOrigPath . '/*') as $__file) {
                    unlink($__file);
                }
            }
        }

        $ava = new BookAuthorAva();
        $ava->setCreatedAt(new \DateTime());

        if (move_uploaded_file($tmpFileName, $fullFileName)) {
            $ava->setFilename($targetFileName);
        } else {
            throw new \Exception('не удалось переместить файл в методе ' . __CLASS__ . '::' . __METHOD__);
        }

        //  открываем изображение
        $origImage = new \SC\MediaBundle\Image\MetaData();
        $origImage->setFileName($fullFileName);

        $targetFileName = $localUploadPath . '/ava-' . $author->getId() . '.jpg';

        $cropper = new \SC\MediaBundle\Image\Cropper();
        $cropper->setSourceFileName($fullFileName);

        $cropper->setCropWidth($coords['w']);
        $cropper->setCropHeight($coords['h']);

        $cropper->setCropXoffset($coords['x']);
        $cropper->setCropYoffset($coords['y']);

        $cropper->setTargetWidth(145);
        $cropper->setTargetHeight(145);
        $cropper->setTargetQuality(95);
        $cropper->crop();
        $cropper->save($targetFileName, IMAGETYPE_JPEG);
        unset($cropper);

        $author->setAva($ava);

        return $author;
    }

    public function getAction(Request $request)
    {
        $q = $request->get('q');

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT a FROM SC\BooksBundle\Entity\BookAuthor a WHERE a.name LIKE ?1');
        $query->setParameter(1, '%' . $q . '%');
        $authors = $query->getResult();

        $results = array();
        foreach ($authors as $author) {
            $results[] = array('id' => $author->getId(), 'text' => $author->getName());
        }


        return new JsonResponse($results);
    }


    public function deleteAction(Request $request)
    {
        $authorId = (int)$request->get('author_id');

        $author = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findOneById($authorId);

        if (is_null($author)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такого автора'));
        }

        if (!is_null($author->getId()) && is_integer($author->getId())) {
            //$localPath = WEB_DIRECTORY . '/files/authors/part_' . (floor($author->getId() / 10000)) . '/' . $author->getId();

            /*if (is_dir($localPath)) {
                $this->rrmdir($localPath);
            }*/
        } else {
            return new JsonResponse(array('code' => 'error', 'message' => 'чото пошло не так'));
        }

        $this->getDoctrine()->getManager()->remove($author);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('code' => 'deleted'));
    }

    private function rrmdir($dir)
    {
        foreach (glob($dir . '/*') as $file) {

            if (is_dir($file)) {
                $this->rrmdir($file);
            } else {
                unlink($file);
            }

        }
        rmdir($dir);
    }

}
