<?php

namespace SC\BooksAdminBundle\Controller;

use SC\BooksAdminBundle\Form\BookType;
use SC\BooksBundle\Entity\Book;
use SC\BooksBundle\Entity\BookCover;
use SC\BooksBundle\Entity\BookFiles;
use SC\CategoryBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BooksController extends Controller
{

    public function badTomesAction(Request $request) {

        $books = $this->getDoctrine()->getRepository("SCBooksBundle:Book")->findBadTomes();

        return $this->render('SCBooksAdminBundle:Books:bad.html.twig', [
            'books' => $books,
        ]);
    }

    public function indexAction(Request $request)
    {

        if ($request->get('q') != null) {
            //$books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBy([], array('createdAt' => 'DESC'));
            $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->searchByTitle($request->get('q'));

        } else if ($request->get('language') != null) {
            $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->searchByLanguage($request->get('language'));
        } else {
            $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findAllBooks();
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $books,
            $request->query->get('page', 1),
            $limit = 20
        );


        $data = array();
        $data['q'] = '';
        $data['pagination'] = $pagination;

        return $this->render('SCBooksAdminBundle:Books:index.html.twig', $data);
    }

    public function statAction(Request $request)
    {
        $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findAllBooks();

        $countData = [
            'booksTotal' => 0,
            'booksPublished' => 0,
            'filesTotal' => 0,
            'filesPublished' => 0,
        ];


        foreach ($books as $book) {
            $countData['booksTotal'] += 1;
            foreach ($book->getBookFile() as $file) {
                $countData['filesTotal'] += 1;
            }

            if ($book->getIsPublished()) {
                $countData['booksPublished'] += 1;
                foreach ($book->getBookFile() as $file) {
                    $countData['filesPublished'] += 1;
                }
            }
        }
        $data['bookStats'] = $countData;

        return $this->render('SCBooksAdminBundle:Books:stat.html.twig', $data);

    }

    public function newAction(Request $request)
    {
        $book = new Book();
        $book->setTitle('');
        $book->setAnnotation('');

        $book->setCreatedAt(new \DateTime());

        $this->getDoctrine()->getManager()->persist($book);
        $this->getDoctrine()->getManager()->flush();

        return $this
            ->redirect(
                $this
                    ->generateUrl('sc_books_admin_edit',
                        array('id' => $book->getId())));
    }


    public function editAction(Request $request, Book $book)
    {

        if (is_null($book)) {
            return new Response('нет такого айди', 404);
        }

        $form = $this->createForm(BookType::class, $book, [

        ]);


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                /*foreach ($book->getBookAuthor() as $author) {
                    $author->addBook($book);
                }*/
                //$book->setSlug($this->createSlug());

                $em = $this->getDoctrine()->getManager();
                $em->persist($book);
                $em->flush();
            }
        }


        return $this->render('SCBooksAdminBundle:Books:editNew.html.twig', [
            'book' => $book,
            'form' => $form->createView(),
        ]);

    }

    public function saveAction(Request $request)
    {
        $id = (int)$request->get('book_id');

        if ($id == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой книги'));
        }

        $book = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findOneById($id);

        if (is_null($book)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой книги'));
        }

        $title = $request->get('book_title');

        if (strlen($title) == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'у книги должно быть название'));
        }

        $book->setUpdatedAt(new \DateTime());
        $book->setTitle($title);
        $book->setOrigin($this->getRequest()->get('book_origin'));

        $isbns = $this->getRequest()->get('book_isbns');

        $isbns = explode(',', $isbns);
        $book->setIsbns($isbns);
        $book->setAnnotation($this->getRequest()->get('book_annotation'));

        $isPublished = !is_null($this->getRequest()->get('book_is_published')) ? true : false;
        $isRecommended = !is_null($this->getRequest()->get('book_is_recommended')) ? true : false;

        $book->setIsPublished($isPublished);
        $book->setIsRecommended($isRecommended);

        $book->setPrimaryCategory($this->getRequest()->get('book_primaryCategory'));

        $cats = explode(',', $this->getRequest()->get('book_category'));

        if (!isset($cats[0]) || (int)$cats[0] == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'у книги должен быть основной раздел'));
        }

        $primaryCat = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById((int)$cats[0]);

        if (is_null($primaryCat)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'у книги должен быть основной раздел'));
        }

        $book->setPrimaryCategory($primaryCat);

        $secondaryCat = null;
        if (isset($cats[1]) and (int)$cats[1] != 0) {
            $secondaryCat = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById((int)$cats[1]);
        }

        foreach ($book->getSecondaryCategories() as $__secondCat) {
            $book->removeSecondaryCategorie($__secondCat);
        }

        if (!is_null($secondaryCat)) {
            $book->addSecondaryCategorie($secondaryCat);
        }

        $authorIds = $this->getRequest()->get('book_authors');

        $authorIds = explode(',', $authorIds);

        //удаляем всех старых авторов
        foreach ($book->getAuthors() as $__author) {
            $book->removeAuthor($__author);
        }


        foreach ($authorIds as $authorId) {
            $author = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findOneById($authorId);
            if (!is_null($author)) {
                $book->addAuthor($author);
            }
        }

        $book->setPublishingHouse($this->getRequest()->get('book_publishing_house'));
        $book->setYear($this->getRequest()->get('book_year'));
        $book->setPages($this->getRequest()->get('book_pages'));
        $book->setTranslator($this->getRequest()->get('book_translator'));

        $lang = $this->getDoctrine()->getRepository('SCBooksBundle:BookLang')->findOneById($this->getRequest()->get('book_lang'));

        $book->setLang($lang);

        if (isset($_FILES['book_cover'])) {
            $this->saveCover($_FILES['book_cover'], $book);
        } else {
            $book->setCover($book->getCover());
        }

        $this->getDoctrine()->getManager()->persist($book);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('code' => 'saved'));
    }


    public function togglePublishAction(Request $request, Book $book)
    {
        if (!$book)
            throw new NotFoundHttpException();

        $book->setIsPublished(!$book->getIsPublished());
        $em = $this->getDoctrine()->getManager();
        $em->persist($book);
        $em->flush();

        return new JsonResponse([
            'status' => $book->getIsPublished()
        ]);
    }

    public function deleteAction(Request $request)
    {
        $id = (int)$this->getRequest()->get('book_id');

        if ($id == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой книги'));
        }

        $book = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findOneById($id);

        if (is_null($book)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой книги'));
        }

        if (!is_null($book->getId()) && is_integer($book->getId())) {
            $localPath = WEB_DIRECTORY . '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId();

            if (is_dir($localPath)) {
                $this->rrmdir($localPath);
            }
        } else {
            return new JsonResponse(array('code' => 'error', 'message' => 'чото пошло не так'));
        }

        $this->getDoctrine()->getManager()->remove($book);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('code' => 'deleted'));
    }

    protected function saveCover(Request $request, $file, $book)
    {
        //1. сохранить оригинал
        $targetFileName = $file['name'];
        $tmpFileName = $file['tmp_name'];

        $localUploadPath = WEB_DIRECTORY . '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId();
        $localPreviewsPath = $localUploadPath . '/cover';

        $localOrigPreviewsPath = $localUploadPath . '/cover/orig';

        if (!file_exists($localUploadPath)) {
            mkdir($localUploadPath, 0777, true);
        }

        if (!file_exists($localPreviewsPath)) {
            mkdir($localPreviewsPath, 0777, true);
        }

        if (!file_exists($localOrigPreviewsPath)) {
            mkdir($localOrigPreviewsPath, 0777, true);
        }

        $extension = pathinfo($targetFileName, PATHINFO_EXTENSION);

        $fullFileName = $localOrigPreviewsPath . '/' . $book->getId() . '_orig.' . $extension;

        if (move_uploaded_file($tmpFileName, $fullFileName)) {
            $cover = $book->getCover();
            if (is_null($cover)) {
                $cover = new BookCover();
                $cover->setCreatedAt(new \DateTime());
                $cover->setFilename($book->getId() . '.jpg');
            } else {
                $cover->setCreatedAt(new \DateTime());
                $cover->setFilename($book->getId() . '.jpg');
            }

            $book->setCover($cover);
            $this->getDoctrine()->getManager()->persist($book);
            $this->getDoctrine()->getManager()->flush();
        } else {
            throw new \Exception('не удалось переместить файл в методе ' . __CLASS__ . '::' . __METHOD__);
        }


        //2. настрокать ресайзов
        //  открываем изображение
        //$origImage = new \SC\MediaBundle\Image\MetaData();
        //$origImage->setFileName($fullFileName);
        //$width = $origImage->getWidth();
        //$height = $origImage->getHeight();

        if (file_exists($fullFileName)) {
            //foreach (array('middle' => 320, 'big' => 600) as $sizeTitle => $previewWidth) {
            //$previewHeigth = \SC\MediaBundle\Image\Service::calculateHeightResized($previewWidth, $width, $height);
            $previewWidth = 120;
            $previewHeigth = 180;
            $targetFileName = $localPreviewsPath . '/' . $book->getId() . '.jpg';

            $resampler = new \SC\MediaBundle\Image\Resampler();
            $resampler->setTargetQuality(90);
            $resampler->setSourceFileName($fullFileName);
            $resampler->setTargetHeight($previewHeigth);
            $resampler->setTargetWidth($previewWidth);

            $resampler->resample();

            $resampler->save($targetFileName, IMAGETYPE_JPEG);
            //}
        }

    }


    public function uploadAction(Request $request)
    {
        // A list of permitted file extensions
        //$allowed = array('png', 'jpg', 'gif','zip');
        $bookId = (int)$request->get('book_id');

        if ($bookId == 0) {
            echo 'errror, no book id';
            exit;
        }

        $book = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findOneById($bookId);

        if (is_null($book)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой книги'));
        }

        if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {

            $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

            $filenameWOExt = $this->createSlug(str_replace('.' . $extension, '', $_FILES['upl']['name']));
            $filename = $filenameWOExt . '.' . $extension;

            $localPath = WEB_DIRECTORY . '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId();

            if (!file_exists($localPath)) {
                mkdir($localPath, 0777, true);
            }

            $filenameWithPath = $localPath . '/' . $filename;

            if (move_uploaded_file($_FILES['upl']['tmp_name'], $filenameWithPath)) {
                $fileinfo = new \SplFileInfo($filenameWithPath);

                $file = new BookFiles();
                $file->setCreatedAt(new \DateTime());
                $file->setFilename($filename);

                $file->setMediaTypeId(\SC\BooksBundle\Constants\MediaTypes::getTypeIdByExt($fileinfo->getExtension()));
                $file->setSize($fileinfo->getSize());

                $book->addFile($file);

                $this->getDoctrine()->getManager()->persist($book);
                $this->getDoctrine()->getManager()->flush();

                echo '{"status":"success", "file_id":"' . $file->getId() . '"}';
                exit;
            }
        }

        echo '{"status":"error"}';
        exit;
    }

    public function fileDeleteAction(Request $request)
    {
        $bookId = (int)$request->get('book_id');

        if ($bookId == 0) {
            echo 'errror, no book id';
            exit;
        }

        $book = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findOneById($bookId);

        if (is_null($book)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой книги'));
        }

        $fileId = (int)$request->get('file_id');

        if ($fileId == 0) {
            echo 'errror, no file id';
            exit;
        }

        $file = $this->getDoctrine()->getRepository('SCBooksBundle:BookFiles')->findOneById($fileId);

        if (is_null($file)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такого файла'));
        }

        $localPath = WEB_DIRECTORY . '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId();

        $filenameWithPath = $localPath . '/' . $file->getFilename();

        if (file_exists($filenameWithPath) && !is_dir($filenameWithPath)) {
            unlink($filenameWithPath);
        }
        $book->removeFile($file);
        $this->getDoctrine()->getManager()->persist($book);
        $this->getDoctrine()->getManager()->flush();

        $this->getDoctrine()->getManager()->remove($file);
        $this->getDoctrine()->getManager()->flush();


        return new JsonResponse(array('code' => 'delete'));
    }


    private function createSlug($slug_string)
    {
        $slug_string = trim($slug_string);

        $t = \Transliterator::create("latin; NFKD; [^\u0000-\u007E] Remove; NFC");
        $slug_string = mb_strtolower($slug_string, 'utf8');
        $slug_string = $t->transliterate($slug_string);

//        $slug_string = \transliterate($slug_string, array(
//            "cyrillic_lowercase",
//            "latin_lowercase",
//            'han_transliterate',
//            'diacritical_remove',
//            'cyrillic_transliterate'
//        ), 'utf-8', 'utf-8');

        $slug_string = strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $slug_string));

        $code_entities_match = array(
            ' ',
            '--',
            '&quot;',
            '!',
            '@',
            '#',
            '$',
            '%',
            '^',
            '&',
            '*',
            '(',
            ')',
            '_',
            '+',
            '{',
            '}',
            '|',
            ':',
            '"',
            '<',
            '>',
            '?',
            '[',
            ']',
            '\\',
            ';',
            "'",
            ',',
            '.',
            '/',
            '*',
            '+',
            '~',
            '`',
            '='
        );
        $code_entities_replace = array(
            '-',
            '-',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $slug_string = str_replace($code_entities_match, $code_entities_replace, $slug_string);
        $slug_string = preg_replace("/-+/", "-", $slug_string);

        return $slug_string;
    }


    private function rrmdir($dir)
    {
        foreach (glob($dir . '/*') as $file) {

            if (is_dir($file)) {
                $this->rrmdir($file);
            } else {
                unlink($file);
            }

        }
        rmdir($dir);
    }
}
