<?php

namespace SC\BooksAdminBundle\Form;

use SC\BooksBundle\Entity\Book;
use SC\BooksBundle\Entity\BookAuthor;
use SC\BooksBundle\Entity\BookFile;
use SC\BooksBundle\Entity\BookLang;
use SC\CategoryBundle\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Название',
                'required' => true,
            ])
            ->add('bookAuthor', EntityType::class, [
                'class' => BookAuthor::class,
                'label' => 'Авторы',
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => false,
                'by_reference' => false,
                'mapped' => true,
                'required' => false
            ])
            ->add('primaryCategory', EntityType::class, [
                'class' => Category::class,
                'label' => 'Раздел',
                'choice_label' => 'title',
                'multiple' => false,
                'expanded' => false,
                'query_builder' => function (EntityRepository $em) {
                    return $em->createQueryBuilder("c")
			->where("c.isPrimary = 1");
                }
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => 'Категория',
                'choice_label' => 'title',
                'multiple' => true,
                'expanded' => false,
                'query_builder' => function (EntityRepository $em) {
                    return $em->createQueryBuilder("c")
                        ->where("c.isPrimary = 0");
                }
            ])
//            ->add('lang', EntityType::class, [
//                'class' => BookLang::class,
//                'label' => 'Язык',
//                'choice_label' => 'title',
//                'multiple' => false,
//                'expanded' => false,
//            ])

            ->add('lang', TextType::class, [
                'label' => 'Язык',
                'required' => false,
            ])
            ->add('bookFile', CollectionType::class, [
                'entry_type' => BookFileType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'prototype' => true,
                'by_reference' => false,
                'label' => 'Тома',
            ])
            ->add('coverFile', FileType::class, [
                'label' => 'Обложка',
                'required' => false,
            ])
            ->add('year', TextType::class, [
                'label' => 'Год',
                'required' => false,
            ])
            ->add('pages', TextType::class, [
                'label' => 'Страниц',
                'required' => false,
            ])
            ->add('publishingHouse', TextType::class, [
                'label' => 'Издательство',
                'required' => false,
            ])
            ->add('annotation', TextareaType::class, [
                'label' => 'Аннотация',
                'required' => true,
                'attr' => [
                    'rows' => 8
                ],
            ])
            ->add('isPublished', CheckboxType::class, [
                'label' =>'Опубликована',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить'
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'book';
    }
}
