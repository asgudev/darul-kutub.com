<?php

namespace SC\BooksAdminBundle\Form;

use SC\BooksBundle\Entity\BookFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bookFileName', TextType::class, [
                'label' => 'Имя тома',
            ])
            ->add('bookPDFFile', FileType::class, [
                'label' => 'PDF',
                'required' => false,
                'file_path' => "bookPDF",
                'file_date' => "createdAt",
            ])
            ->add('bookFB2File', FileType::class, [
                'label' => 'FB2',
                'required' => false,
                'file_path' => "bookFB2",
                'file_date' => "createdAt",

            ])
            ->add('bookEPUBFile', FileType::class, [
                'label' => 'EPUB',
                'required' => false,
                'file_path' => "bookEPUB",
                'file_date' => "createdAt",

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BookFile::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'book_file';
    }
}
