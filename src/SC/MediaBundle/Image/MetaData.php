<?php
namespace SC\MediaBundle\Image;

class MetaData
{
    protected $fileName;
    protected $width;
    protected $height;
    protected $imageTypeId = null;

    protected $parsed = false;

    public function getImageTypeId()
    {
        if (!$this->parsed) {
            $this->parse();
        }

        return $this->imageTypeId;
    }

    public function setImageTypeId($imageTypeId)
    {

        $this->imageTypeId = $imageTypeId;
    }

    /**
     * @return unknown
     */
    public function getFileName()
    {
        if (is_null($this->fileName)) {
            throw new \Exception('имя файла не было установлено');
        }

        return $this->fileName;
    }

    /**
     * @param unknown_type $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return unknown
     */
    public function getHeight()
    {
        if (!$this->parsed) {
            $this->parse();
        }

        return $this->height;
    }

    /**
     * @param unknown_type $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return unknown
     */
    public function getWidth()
    {
        if (!$this->parsed) {
            $this->parse();
        }
        return $this->width;
    }

    /**
     * @param unknown_type $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    private function parse()
    {
        $this->parsed = true;

        $res = getimagesize($this->getFileName());

        if ($res !== false) {
            $this->width = $res[0];
            $this->height = $res[1];
            $this->imageTypeId = $res[2];
        } else {
            throw new \Exception('Не вышло получить данные по картинке ' . $this->fileName . ' - видимо это не картинка вовсе');
        }
    }
}