<?php
namespace SC\MediaBundle\Image;

class Service
{
    public static function calculateHeightResized($newWidth, $originalWidth, $originalHeight)
    {
        $ko = $originalWidth / $originalHeight;
        return (int)($newWidth / $ko);
    }
}