<?php
namespace SC\MediaBundle\Image;

class TextWriter extends ImageAbstract
{
    protected $targetQuality = 75;

    public function setTargetQuality($targetQuality)
    {
        $this->targetQuality = $targetQuality;
    }

    public function getTargetQuality()
    {
        return $this->targetQuality;
    }

    public function writeText($targetFileName, $text)
    {
        putenv('GDFONTPATH=' . WEB_DIRECTORY . '/bundles/scarticleadmin/fonts/');

        $image = $this->getSourceImgRes();
        $imageMetaData = $this->getSourceMetaData();

        imagefilledrectangle($image, 5, 5, $imageMetaData->getWidth() - 5, 22 * sizeof(explode("\n", $text)), imagecolorallocatealpha($image, 255, 255, 255, 30));
        imagefttext($image, '10', 0, 10, 20, imagecolorallocatealpha($image, 0, 0, 0, 0), 'tahoma', $text);

        if (file_exists($targetFileName)) {
            unlink($targetFileName);
        }

        imagejpeg($image, $targetFileName, $this->getTargetQuality());
        imagedestroy($image);
    }
}
