<?php
namespace SC\MediaBundle\Image;

abstract class ImageAbstract
{
    protected $contentTypes = array(
        IMAGETYPE_GIF => 'gif',
        IMAGETYPE_JPEG => 'jpeg',
        IMAGETYPE_PNG => 'png',
        IMAGETYPE_SWF => 'swf',
        IMAGETYPE_PSD => 'psd',
        IMAGETYPE_BMP => 'bmp',
        IMAGETYPE_TIFF_II => 'tiff',
        IMAGETYPE_TIFF_MM => 'tiff',
        IMAGETYPE_JPC => 'jpc',
        IMAGETYPE_JP2 => 'jp2',
        IMAGETYPE_JPX => 'jpx',
        IMAGETYPE_JP2 => 'jb2',
        IMAGETYPE_SWC => 'swc',
        IMAGETYPE_IFF => 'iff',
        IMAGETYPE_WBMP => 'wmp',
        IMAGETYPE_XBM => 'xmb'
    );

    protected $loaderTypes = array(
        IMAGETYPE_GIF => 'gif',
        IMAGETYPE_JPEG => 'jpeg',
        IMAGETYPE_JPEG2000 => 'jpeg',
        IMAGETYPE_PNG => 'png'
    );

    protected $sourceFileName = null;
    protected $sourceImgRes = null;
    protected $targetImgRes = null;
    protected $sourceMetaData = null;

    abstract public function getTargetQuality();

    public function getSourceFileName()
    {
        if (is_null($this->sourceFileName)) {
            throw new \Exception('Source fileName is not defined');
        }

        return $this->sourceFileName;
    }

    public function setSourceFileName($sourceFileName)
    {
        $this->sourceFileName = $sourceFileName;
    }

    protected function getSourceMetaData()
    {
        if (is_null($this->sourceMetaData)) {
            $meta = new MetaData();
            $meta->setFileName($this->getSourceFileName());
            $this->sourceMetaData = $meta;
        }

        return $this->sourceMetaData;
    }

    protected function getSourceImgRes()
    {

        if (is_null($this->sourceImgRes)) {
            if (isset($this->loaderTypes[$this->getSourceMetaData()->getImageTypeId()])) {
                $createFunction = 'imagecreatefrom' . $this->loaderTypes[$this->getSourceMetaData()->getImageTypeId()];
            } else {
                throw new \Exception('Этот тип картинок пока не поддерживается');
            }

            $this->sourceImgRes = $createFunction($this->getSourceFileName());
        }

        return $this->sourceImgRes;
    }

    public function save($fileName, $imageType = IMAGETYPE_JPEG)
    {
        /**
         * Сейвер выбирается не по экстеншну $fileName, потому
         * что иногда мы генерим картинки без экстеншна - временные картинки например
         */
        if (is_null($this->targetImgRes)) {
            throw new \Exception('Target img is not a resource');
        }

        switch ($imageType) {
            case IMAGETYPE_PNG:
                imagepng($this->targetImgRes, $fileName);
                break;
            case IMAGETYPE_GIF:
                imagegif($this->targetImgRes, $fileName);
                break;
            case IMAGETYPE_JPEG || IMAGETYPE_JPEG2000:
                imagejpeg($this->targetImgRes, $fileName, $this->getTargetQuality());
                break;
            default:
                throw new \Exception('Unsupported image type, cannot send it to browser');
        }
    }

    public function sendToBrowser()
    {
        if (is_null($this->targetImgRes)) {
            throw new \Exception('TargetImg is not a resource');
        }

        header('Content-type: image/' . $this->contentTypes[$this->getSourceMetaData()->getImageTypeId()]);

        switch ($this->getSourceMetaData()->getImageTypeId()) {
            case IMAGETYPE_PNG:
                imagepng($this->targetImgRes, null);
                break;
            case IMAGETYPE_GIF:
                imagegif($this->targetImgRes, null);
                break;
            case IMAGETYPE_JPEG || IMAGETYPE_JPEG2000:
                imagejpeg($this->targetImgRes, null, $this->getTargetQuality());
                break;
            default:
                throw new \Exception('Unsupported image type, cannot send it to browser');
        }
    }
}