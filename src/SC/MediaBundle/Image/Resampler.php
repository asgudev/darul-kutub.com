<?php
namespace SC\MediaBundle\Image;

class Resampler extends ImageAbstract
{
    protected $targetWidth;
    protected $targetHeight;

    protected $targetQuality = 75;

    public function getTargetQuality()
    {
        return $this->targetQuality;
    }

    public function setTargetQuality($targetQuality)
    {
        $this->targetQuality = $targetQuality;
    }

    public function getTargetHeight()
    {
        if (is_null($this->targetHeight)) {
            throw new \Exception('Target height is not defined');
        }

        return $this->targetHeight;
    }

    public function setTargetHeight($targetHeight)
    {
        $this->targetHeight = $targetHeight;
    }

    public function getTargetWidth()
    {
        if (is_null($this->targetWidth)) {
            throw new \Exception('Target width is not defined');
        }

        return $this->targetWidth;
    }

    public function setTargetWidth($targetWidth)
    {
        $this->targetWidth = $targetWidth;
    }

    public function resample()
    {
        $this->targetImgRes = imagecreatetruecolor($this->getTargetWidth(), $this->getTargetHeight());
        return imagecopyresampled($this->targetImgRes, $this->getSourceImgRes(), 0, 0, 0, 0, $this->getTargetWidth(), $this->getTargetHeight(), $this->getSourceMetaData()->getWidth(), $this->getSourceMetaData()->getHeight());
    }
}