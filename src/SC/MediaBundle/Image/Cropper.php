<?php
namespace SC\MediaBundle\Image;

class Cropper extends ImageAbstract
{

    protected $cropWidth;
    protected $cropHeight;
    protected $cropXoffset;
    protected $cropYoffset;

    protected $targetQuality = 75;
    protected $targetWidth;
    protected $targetHeight;

    public function getTargetHeight()
    {
        return $this->targetHeight;
    }

    public function setTargetHeight($targetHeight)
    {
        $this->targetHeight = $targetHeight;
    }

    public function getTargetWidth()
    {
        return $this->targetWidth;
    }

    public function setTargetWidth($targetWidth)
    {
        $this->targetWidth = $targetWidth;
    }

    public function getCropHeight()
    {
        return $this->cropHeight;
    }

    public function setCropHeight($cropHeight)
    {
        $this->cropHeight = $cropHeight;
    }

    public function getCropWidth()
    {
        return $this->cropWidth;
    }

    public function setCropWidth($cropWidth)
    {
        $this->cropWidth = $cropWidth;
    }

    public function getCropXoffset()
    {
        return $this->cropXoffset;
    }

    public function setCropXoffset($cropXoffset)
    {
        $this->cropXoffset = $cropXoffset;
    }

    public function getCropYoffset()
    {
        return $this->cropYoffset;
    }

    public function setCropYoffset($cropYoffset)
    {
        $this->cropYoffset = $cropYoffset;
    }

    public function getTargetQuality()
    {
        return $this->targetQuality;
    }

    public function setTargetQuality($targetQuality)
    {
        $this->targetQuality = $targetQuality;
    }

    public function crop()
    {
        $metaData = $this->getSourceMetaData();

        $this->targetImgRes = imagecreatetruecolor($this->getTargetWidth(), $this->getTargetHeight());

        return imagecopyresampled($this->targetImgRes, $this->getSourceImgRes(), 0, 0, $this->getCropXoffset(), $this->getCropYoffset(), $this->getTargetWidth(), $this->getTargetHeight(), $this->getCropWidth(), $this->getCropHeight());
    }
}