<?php

namespace SC\CategoryAdminBundle\Controller;

use SC\CategoryBundle\Entity\Category;
use SC\CategoryBundle\Entity\CategoryMeta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findAll();

        $sorted = array();
        foreach ($categories as $cat) {
            if ($cat->getLevel() == 1) {
                $sorted[$cat->getId()]['parent'] = $cat;
                foreach ($categories as $subCat) {
                    if ($cat->getId() == $subCat->getParentId()) {
                        $sorted[$cat->getId()]['subs'][] = $subCat;
                    }
                }
            }
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $sorted,
            $request->query->get('page', 1),
            $limit = 20
        );

        $data = array();
        $data['pagination'] = $pagination;

        $template = $this->container->getParameter('sc_category_admin.templates.index');
        return $this->render($template, $data);
    }

    public function newAction(Request $request)
    {
        $category = new Category();
        $meta = new CategoryMeta();
        $category->setMeta($meta);

        $this->getDoctrine()->getManager()->persist($category);
        $this->getDoctrine()->getManager()->flush();
        return $this
            ->redirect(
                $this
                    ->generateUrl('sc_category_admin_edit',
                        array('id' => $category->getId())));
    }


    public function editAction(Request $request, $id)
    {
        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById($id);

        if (is_null($category)) {
            return new Response('нет такого айди', 404);
        }

        $firstLevelCategories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findByLevel(1);

        $picUrl = $localUploadPath = '/files/categories/part_' . (floor($category->getId() / 10000)) . '/' . $category->getId() . '/' . $category->getId() . '.jpg';
        $category->setPicUrl($picUrl);

        $template = $this->container->getParameter('sc_category_admin.templates.edit');
        $width = $this->container->getParameter('sc_category_admin.sizes.width');
        $height = $this->container->getParameter('sc_category_admin.sizes.height');
        $ratio = $this->container->getParameter('sc_category_admin.sizes.ratio');

        return $this->render($template, array('category' => $category,
            'firstLevelCats' => $firstLevelCategories,
            'width' => $width, 'height' => $height, 'ratio' => $ratio));
    }

    public function saveAction(Request $request)
    {
        $id = (int)$request->get('category_id');

        if ($id == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой категории'));
        }

        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById($id);

        if (is_null($category)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой категории'));
        }

        $title = $request->get('category_title');

        if (strlen($title) == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'название должно быть'));
        }

        $category->setTitle($title);

        $parentId = $request->get('category_parent_id');

        $category->setParentId($parentId);

        /*
         * todo: здесь тупая логика, которая учитывает только 2 уровня
         */
        if ($parentId == 0) {
            $category->setLevel(1);
        } else {
            $category->setLevel(2);
        }

        $isPrimary = !is_null($request->get('category_is_primary')) ? true : false;

        $category->setIsPrimary($isPrimary);

        $path = $request->get('category_path');

        $category->setDescr($request->get('category_descr'));

        if (empty($path)) {
            $category->setPath($this->createSlug($title));
        } else {
            $category->setPath($path);
        }

        $category->getMeta()->setTitle($request->get('category_meta_title'));
        $category->getMeta()->setKeywords($request->get('category_meta_keywords'));
        $category->getMeta()->setDescr($request->get('category_meta_descr'));


        if (isset($_FILES['category_pic']) && $request->get('coords')['x'] != '' && $request->get('coords')['y'] != '') {
            $this->savePic($_FILES['category_pic'], $category, $request->get('coords'));
        }


        $this->getDoctrine()->getManager()->persist($category);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('code' => 'saved'));

    }

    public function savePic(Request $request, $file, $category, $coords)
    {
        //1. сохранить оригинал
        $targetFileName = $file['name'];
        $tmpFileName = $file['tmp_name'];

        $localUploadPath = WEB_DIRECTORY . '/files/categories/part_' . (floor($category->getId() / 10000)) . '/' . $category->getId();
        $localOrigPath = $localUploadPath . '/orig';

        if (!file_exists($localUploadPath)) {
            mkdir($localUploadPath, 0777, true);
        }

        if (!file_exists($localOrigPath)) {
            mkdir($localOrigPath, 0777, true);
        }

        foreach (glob($localOrigPath . '/*') as $__file) {
            unlink($__file);
        }

        $fullFileName = $localOrigPath . '/' . $targetFileName;

        if (!move_uploaded_file($tmpFileName, $fullFileName)) {
            throw new \Exception('не удалось переместить файл в методе ' . __CLASS__ . '::' . __METHOD__);
        }

        //  открываем изображение
        $origImage = new \SC\MediaBundle\Image\MetaData();
        $origImage->setFileName($fullFileName);

        $targetFileName = $localUploadPath . '/' . $category->getId() . '.jpg';

        $cropper = new \SC\MediaBundle\Image\Cropper();
        $cropper->setSourceFileName($fullFileName);

        $cropper->setCropWidth($coords['w']);
        $cropper->setCropHeight($coords['h']);

        $cropper->setCropXoffset($coords['x']);
        $cropper->setCropYoffset($coords['y']);


        $width = $this->container->getParameter('sc_category_admin.sizes.width');
        $height = $this->container->getParameter('sc_category_admin.sizes.height');

        $cropper->setTargetWidth($width);
        $cropper->setTargetHeight($height);
        $cropper->setTargetQuality(95);
        $cropper->crop();
        $cropper->save($targetFileName, IMAGETYPE_JPEG);
        unset($cropper);

        $category->setHasPic(true);
    }


    public function deleteAction(Request $request)
    {
        $id = (int)$request->get('category_id');

        if ($id == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой категории'));
        }

        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById($id);

        if (is_null($category)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'нет такой категории'));
        }
/*
        if (!is_null($category->getId()) && is_integer($category->getId())) {
            $localPath = WEB_DIRECTORY . '/files/categories/part_' . (floor($category->getId() / 10000)) . '/' . $category->getId();

            if (is_dir($localPath)) {
                $this->rrmdir($localPath);
            }
        } else {
            return new JsonResponse(array('code' => 'error', 'message' => 'чото пошло не так'));
        }
*/

        $this->getDoctrine()->getManager()->remove($category);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('code' => 'deleted'));
    }

    private function createSlug($slug_string)
    {
        $slug_string = trim($slug_string);

        $t = \Transliterator::create("latin; NFKD; [^\u0000-\u007E] Remove; NFC");
        $slug_string = mb_strtolower($slug_string, 'utf8');
        $slug_string = $t->transliterate($slug_string);


//        $slug_string = \transliterate($slug_string, array(
//            "cyrillic_lowercase",
//            "latin_lowercase",
//            'han_transliterate',
//            'diacritical_remove',
//            'cyrillic_transliterate'
//        ), 'utf-8', 'utf-8');

        $slug_string = strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $slug_string));

        $code_entities_match = array(
            ' ',
            '--',
            '&quot;',
            '!',
            '@',
            '#',
            '$',
            '%',
            '^',
            '&',
            '*',
            '(',
            ')',
            '_',
            '+',
            '{',
            '}',
            '|',
            ':',
            '"',
            '<',
            '>',
            '?',
            '[',
            ']',
            '\\',
            ';',
            "'",
            ',',
            '.',
            '/',
            '*',
            '+',
            '~',
            '`',
            '='
        );
        $code_entities_replace = array(
            '-',
            '-',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $slug_string = str_replace($code_entities_match, $code_entities_replace, $slug_string);
        $slug_string = preg_replace("/-+/", "-", $slug_string);

        return $slug_string;
    }

    private function rrmdir($dir)
    {
        foreach (glob($dir . '/*') as $file) {

            if (is_dir($file)) {
                $this->rrmdir($file);
            } else {
                unlink($file);
            }

        }
        rmdir($dir);
    }

}
