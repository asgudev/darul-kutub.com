<?php

namespace SC\CategoryAdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sc_category_admin');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $this->addTemplatesSection($rootNode);
        $this->addPreviewSizes($rootNode);

        return $treeBuilder;
    }


    private function addTemplatesSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('templates')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('index')
            ->defaultValue('SCCategoryAdminBundle:Default:index.html.twig')
            ->end()
            ->scalarNode('edit')
            ->defaultValue('SCCategoryAdminBundle:Default:edit.html.twig')
            ->end()
            ->end()
            ->end()
            ->end();
    }

    private function addPreviewSizes(ArrayNodeDefinition $node) {
        $node
            ->children()
            ->arrayNode('sizes')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('width')
            ->defaultValue(145)
            ->end()
            ->scalarNode('height')
            ->defaultValue(145)
            ->end()
            ->scalarNode('ratio')
            ->defaultValue(1)
            ->end()
            ->end()
            ->end()
            ->end();
    }
}
