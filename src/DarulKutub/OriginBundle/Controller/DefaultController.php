<?php

namespace DarulKutub\OriginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->redirectToRoute('sc_books_admin');

    }

    public function clearCacheAction(Request $request)
    {
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput(array(
            'command' => 'cache:clear',
            '--env' => $this->getParameter('kernel.environment')
        ));
        $application->run($input);

        return new JsonResponse();
    }

    public function buildCatalogAction(Request $request)
    {
        $catalogs = [
            '11' => 'files/catalog.xlsx',
            '30' => 'files/catalog_modern.xlsx',
        ];
        foreach ($catalogs as $catId => $fileName) {
            $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findAllBooksByCat($catId);

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            $rowCount = 1;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Название');
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Автор');
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'URL');

            foreach ($books as $book) {
                $authors = [];
                $rowCount++;


                foreach ($book->getBookAuthor() as $author) $authors[] = $author->getName();

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $book->getTitle());
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, implode(',', $authors));

                $url = 'http:' . $this->generateUrl('book_show_single', [
                        'book_id' => $book->getId()
                    ]);

                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $url);
                $objPHPExcel->getActiveSheet()->getCell('C' . $rowCount)->getHyperlink()->setUrl($url);
            }

            foreach (range('A', 'C') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setWidth(85);

            }

            $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->save($fileName);
        }

        return new JsonResponse();
    }
}
