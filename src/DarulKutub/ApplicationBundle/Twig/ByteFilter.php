<?php

namespace DarulKutub\ApplicationBundle\Twig;


class ByteFilter extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('byte_format', array($this, 'byteFilter')),
        );
    }

    public function byteFilter($number)
    {
        return $this->format_bytes($number);
    }

    public function getName()
    {
        return 'byte_filter';
    }

    public function format_bytes($bytes, $si = true)
    {
        $unit = $si ? 1000 : 1024;
        if ($bytes <= $unit) return $bytes . " B";
        $exp = intval((log($bytes) / log($unit)));
        $pre = ($si ? "kMGTPE" : "KMGTPE");
        $pre = $pre[$exp - 1] . ($si ? "" : "i");
        return sprintf("%.1f %sB", $bytes / pow($unit, $exp), $pre);
    }
}