<?php

namespace DarulKutub\ApplicationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddModernCategoryCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('darul:update:modern')
            ->setDescription('Update modern categories');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getDoctrine()->getManager();

        $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findWithCategories();

        $modernCat = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->find(30);

        /* @var \SC\BooksBundle\Entity\Book[] $books */
        foreach ($books as $__book) {
            $hasTbnCategory = false;
            $hasModernCategory = false;

            foreach ($__book->getCategory() as $__cat) {
                if ($__cat->getId() == 11) $hasTbnCategory = true;
                if ($__cat->getId() == 30) $hasModernCategory = true;
            }

            if ((!$hasTbnCategory) && (!$hasModernCategory)) $__book->addCategory($modernCat);

            $em->persist($__book);
        }
        $em->flush();
    }

    private function getDoctrine()
    {
        return $this->getContainer()->get('doctrine');
    }
}
