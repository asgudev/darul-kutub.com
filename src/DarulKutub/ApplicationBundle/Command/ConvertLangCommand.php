<?php
namespace DarulKutub\ApplicationBundle\Command;

use SC\BooksBundle\Entity\BookLang;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\Bundle\DoctrineBundle\Registry;


class ConvertLangCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('lang:convert')
            ->setDescription('Convert old address schema');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $books = $this->getContainer()->get('doctrine')->getRepository('SCBooksBundle:Book')->findAll();

        $count = 0;
        foreach ($books as $book) {
            $lang = $this->getContainer()->get('doctrine')->getRepository('SCBooksBundle:BookLang')->findOneByTitle($book->getLangTmp());
            if (is_null($lang)) {
                $lang = new BookLang();
                $lang->setTitle($book->getLangTmp());
                $this->getContainer()->get('doctrine')->getManager()->persist($lang);
                $this->getContainer()->get('doctrine')->getManager()->flush();
                $book->setLang($lang);
                $book->setUpdatedAt(new \DateTime());
                $this->getContainer()->get('doctrine')->getManager()->persist($book);
                $this->getContainer()->get('doctrine')->getManager()->flush();
            } else {
                $book->setLang($lang);
                $book->setUpdatedAt(new \DateTime());
                $this->getContainer()->get('doctrine')->getManager()->persist($book);
                $this->getContainer()->get('doctrine')->getManager()->flush();
            }
            var_dump($count);
            $count++;

        }
    }

}