<?php
namespace DarulKutub\ApplicationBundle\Command;

use SC\ArticleBundle\Entity\ArticleMeta;
use SC\ArticleBundle\Entity\ArticleStats;
use SC\CategoryBundle\Entity\CategoryMeta;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Class for AbstractCommand
 */
class ArticleConvertCommand extends ContainerAwareCommand
{
    /**
     * Configure the command.
     */
    protected function configure()
    {
        $this
            ->setName('article:convert')
            ->setDescription('Convert old address schema');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $articles = $this->getContainer()->get('doctrine')->getRepository('SCArticleBundle:Article')->findAll();

        $user = $this->getContainer()->get('doctrine')->getRepository('SCUserBundle:BaseUser')->findOneByUsername('knigi.darul.kutub@gmail.com');
        $count = 0;
        foreach ($articles as $article) {
            $stat = new ArticleStats();
            $stat->setArticle($article);
            $article->setViewCnt($stat);

            $meta = new ArticleMeta();
            $article->setMeta($meta);
            $article->setAuthor($user);
            $article->setPath($this->createPath($article->getTitle()));
            $this->getContainer()->get('doctrine')->getManager()->persist($article);

            if ($count % 500 == 0) {
                $this->getContainer()->get('doctrine')->getManager()->flush();
            }
            $count++;
            var_dump('article_' . $count);
        }

        $this->getContainer()->get('doctrine')->getManager()->flush();

        $categories = $this->getContainer()->get('doctrine')->getRepository('SCCategoryBundle:Category')->findAll();

        foreach ($categories as $category) {
            $meta = new CategoryMeta();
            $category->setMeta($meta);
            $this->getContainer()->get('doctrine')->getManager()->persist($category);
            $this->getContainer()->get('doctrine')->getManager()->flush();
            var_dump('category_' . $category->getId());
        }
    }

    public function createPath($title)
    {
        $t = \Transliterator::create("latin; NFKD; [^\u0000-\u007E] Remove; NFC");
        $path = $t->transliterate($title);
        $path = strtolower($path);

        $path = preg_replace('/[\'"]+/i', '', $path);
        $path = preg_replace('/[^a-z0-9_]+/i', '-', $path);
        $path = trim($path, '-');

        return $path;
    }
}