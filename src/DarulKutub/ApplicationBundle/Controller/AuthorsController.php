<?php

namespace DarulKutub\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AuthorsController extends Controller
{
    public function showAllAction()
    {
        $letter = $this->getRequest()->get('letter');


        if (!is_null($letter)) {
            $authors = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findByLetter(array('letter' => $this->ordutf8($letter)));
        } else {
            $authors = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findAll();
        }

        foreach (range(176, 176 + 31) as $i) {
            $letters[$i] = iconv('ISO-8859-5', 'UTF-8', chr($i));
        }

        $alphabetAuthors = array();
        foreach ($authors as $author) {
            $__letter = strtoupper($this->uchr($author->getLetter()));
            $alphabetAuthors[$__letter][] = $author;
        }

        ksort($alphabetAuthors);

        return $this->render('DarulKutubApplicationBundle:Authors:showAll.html.twig', array('alphabetAuthors' => $alphabetAuthors, 'letters' => $letters, 'letter' => $letter));
    }

    public function showSingleAction()
    {
        $authorId = (int)$this->getRequest()->get('author_id');


        if ($authorId == 0) {
            return new Response('нет такого автора', 404);
        }

        $author = $this->getDoctrine()->getRepository('SCBooksBundle:BookAuthor')->findOneById($authorId);


        if (is_null($author)) {
            return new Response('нет такого автора', 404);
        }

        $avaUrl = '/files/authors/part_' . (floor($author->getId() / 10000)) . '/' . $author->getId() . '/ava-' . $author->getId() . '.jpg';
        $author->setAvaUrl($avaUrl);

        foreach ($author->getBooks() as $book) {
            $coverUrl = '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId() . '/cover/' . $book->getId() . '.jpg';
            $book->setCoverUrl($coverUrl);
        }

        return $this->render('DarulKutubApplicationBundle:Authors:showSingle.html.twig', array('author' => $author, 'letter' => $this->uchr($author->getLetter())));

    }

    protected function ordutf8($string, $offset = 0)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) $offset = -1;
        return $code;
    }

    protected function uchr ($codes) {
        if (is_scalar($codes)) $codes= func_get_args();
        $str= '';
        foreach ($codes as $code) $str.= html_entity_decode('&#'.$code.';',ENT_NOQUOTES,'UTF-8');
        return $str;
    }
}