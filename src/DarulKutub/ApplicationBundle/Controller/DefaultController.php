<?php

namespace DarulKutub\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findBy([
            'isInMenu' => true,
            'isPrimary' => 0,
        ]);

        $primaryCatId = 11;
        $booksCount = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->getBookCount($primaryCatId);

        return $this->render('DarulKutubApplicationBundle:Default:index.html.twig', [
            'categories' => $categories,
            'bookCount' => $booksCount,
            'primaryCatId' => $primaryCatId

        ]);
    }


    public function aboutAction()
    {
        return $this->render('DarulKutubApplicationBundle:Default:about.html.twig');
    }

    public function policyAction()
    {
        return $this->render('DarulKutubApplicationBundle:Default:policy.html.twig');
    }


    public function getCatalogAction(Request $request)
    {
        $file = $this->get('kernel')->getRootDir() . '/../web/files/catalog.xlsx';
        $response = new BinaryFileResponse($file);

        $d = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'catalog.xlsx'
        );

        $response->headers->set('Content-Disposition', $d);

        return $response;
    }


    public function newBooksAction(Request $request)
    {
        $booksDql = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findNewBooks(11, 10, true)->setHint('knp_paginator.count', 10);
        $translator = $this->get('translator');
        $query = $translator->trans('Новые');

        $pagination = $this->get('knp_paginator')->paginate(
            $booksDql, /* query NOT result */
            $request->query->getInt('page', 1) /* page number */,
            10 /* limit per page */
        );

        return $this->render('DarulKutubApplicationBundle:Default:search.html.twig', [
            'pagination' => $pagination,
            'query' => $query,
        ]);
    }


    public function searchAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $type = $request->get("type");
            $author = $request->get("author");
            $title = $request->get("title");

            if (($title == "" && $author != "") || ($title != "" && $author == "")) {

                if ($author != "") {
                    $results = $this->getDoctrine()->getRepository("SCBooksBundle:BookAuthor")->searchByName($author);
                    foreach ($results as $__res) {
                        $return[] = [
                            'id' => $__res->getId(),
                            'name' => $__res->getName(),
                        ];
                    }
                }

                if ($title != "") {
                    $results = $this->getDoctrine()->getRepository("SCBooksBundle:Book")->searchByTitle($title);
                    foreach ($results as $__res) {
                        $return[] = [
                            'id' => $__res->getId(),
                            'name' => $__res->getTitle(),
                        ];
                    }
                }

            } else if ($title != "" && $author != "") {
                $results = $this->getDoctrine()->getRepository("SCBooksBundle:Book")->searchByAuthorAndTitle($author, $title);

                foreach ($results as $__res) {
                    $return[] = [
                        'id' => $__res->getId(),
                        'name' => $__res->getTitle(),
                    ];
                }
            }



            return new JsonResponse($return);

        } else {
            $author = !is_null($request->get('author')) ? $request->get('author') : null;
            $title = !is_null($request->get('title')) ? $request->get('title') : null;
            $category = !is_null($request->get('category')) ? $request->get('category') : null;
            $combined = !is_null($request->get('q')) ? $request->get('q') : null;
            $order = !is_null($request->get('order')) ? $request->get('order') : 'ASC';
            $lang = $request->get('lang');
            $primaryCatId = !is_null($request->get('primary')) ? $request->get('primary') : null;


            if ($request->get('q')) {
                $query = $request->get('q');

                $booksDql = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBookCombined($query);

            } else {

                if ($category != '') {
                    $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->find($category);
                    $query = trim(implode(' ', [$category->getTitle(), $author, $title]));
                } else {
                    $query = trim(implode(' ', [$author, $title]));
                }

                $booksDql = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBook($title, $author, $category, $primaryCatId, "DESC", true);
            }


            $pagination = $this->get('knp_paginator')->paginate(
                $booksDql, /* query NOT result */
                $request->query->getInt('page', 1) /* page number */,
                100 /* limit per page */
            );


            return $this->render('DarulKutubApplicationBundle:Default:search.html.twig', [
                'query' => $query,
                'pagination' => $pagination,
            ]);
        }
    }


    /*

    $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_books.indexes.main') . ' ' . $this->container->getParameter('sc_books.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(1000, 0);

        $sphinx->setFilter('is_published', array(1));

        $langTitle = 'Поиск';
        if (!is_null($lang)) {
            switch ($lang) {
                case 'russ':
                    $sphinx->setFilter('lang', [6]);
                    $langTitle = 'Книги на русском языке';
                    break;
                case 'tatar':
                    $sphinx->setFilter('lang', [3]);
                    $langTitle = 'Книги на татарском языке';
                    break;
                case 'arab':
                    $sphinx->setFilter('lang', [1]);
                    $langTitle = 'Книги на арабском языке';
                    break;
                default:
                    $langTitle = 'Поиск';
                    break;
            }
        }

        $sphinx->q($q);

        $books = [];
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $parameters = array('id' => $sphinx->getFoundIds());

            $books = $this->getDoctrine()
                ->getRepository('SCBooksBundle:Book')
                ->findBy($parameters, ['createdAt' => 'desc']);
        }

        foreach ($books as $book) {
            $coverUrl = '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId() . '/cover/' . $book->getId() . '.jpg';
            $book->setCoverUrl($coverUrl);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $books,
            $request->query->get('page', 1),
            $limit = 20
        );

        $data['pagination'] = $pagination;
        $data['q'] = $q;
        $data['langTitle'] = $langTitle;




     */
}
