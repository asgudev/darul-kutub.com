<?php

namespace DarulKutub\ApplicationBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ModernController extends Controller
{

    public function modernAction(Request $request)
    {
//return $this->redirect('/');
        $categories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findBy([
            'isInMenu' => true,
            'isPrimary' => 0,

        ]);

        $primaryCatId = 30;
        $booksCount = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->getBookCount($primaryCatId);

        return $this->render('DarulKutubApplicationBundle:Modern:index.html.twig', [
            'categories' => $categories,
            'bookCount' => $booksCount,
            'primaryCatId' => $primaryCatId
        ]);
    }

    public function aboutAction()
    {
//return $this->redirect('/');

        return $this->render('DarulKutubApplicationBundle:Modern:about.html.twig');
    }


    public function newBooksAction(Request $request)
    {
//return $this->redirect('/');

        $booksDql = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findNewBooks(30, 10, true)->setHint('knp_paginator.count', 10);
        $translator = $this->get('translator');
        $query = $translator->trans('Новые');

        $pagination = $this->get('knp_paginator')->paginate(
            $booksDql, /* query NOT result */
            $request->query->getInt('page', 1) /* page number */,
            10 /* limit per page */
        );

        return $this->render('DarulKutubApplicationBundle:Default:search.html.twig', [
            'pagination' => $pagination,
            'query' => $query,
        ]);
    }

    public function getCatalogAction(Request $request)
    {
//return $this->redirect('/');

        $file = $this->get('kernel')->getRootDir() . '/../web/files/catalog_modern.xlsx';
        $response = new BinaryFileResponse($file);

        $d = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'modern.xlsx'
        );

        $response->headers->set('Content-Disposition', $d);

        return $response;
    }
}
