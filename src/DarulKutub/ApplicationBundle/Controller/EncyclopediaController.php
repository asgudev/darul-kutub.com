<?php

namespace DarulKutub\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class EncyclopediaController extends Controller
{
    public function indexAction()
    {
        $letter = $this->getRequest()->get('letter');


        if (!is_null($letter)) {
            $articles = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findByLetter(array('letter' => $this->ordutf8($letter), 'is_published' => true));
        } else {
            $articles = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findAll(array('is_published' => true));
        }

        foreach (range(176, 176 + 31) as $i) {
            $letters[$i] = iconv('ISO-8859-5', 'UTF-8', chr($i));
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $this->get('request')->query->get('page', 1),
            $limit = 10
        );

        $data = array('pagination' => $pagination);
        $data['letters'] = $letters;
        $data['letter'] = $letter;

        return $this->render('DarulKutubApplicationBundle:Encyclopedia:index.html.twig', $data);
    }

    public function singleAction()
    {
        $articleId = (int)$this->getRequest()->get('article_id');

        if ($articleId == 0) {
            return new Response('нет такой статьи', 404);
        }

        $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneById($articleId);

        if (is_null($article)) {
            return new Response('нет такой статьи', 404);
        }


        $this->container->get('sc_article_item.service')->hydrateContent(array($article));
        $this->container->get('sc_article_item.service')->hydrateFlatContent(array($article));

        $this->container->get('sc_article_item.service')->hydrateFiles(array($article));

        if (mb_strlen($article->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($article);
        }


        return $this->render('DarulKutubApplicationBundle:Encyclopedia:single.html.twig', array('article' => $article));
    }

    protected function ordutf8($string, $offset = 0)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) $offset = -1;
        return $code;
    }
}