<?php

namespace DarulKutub\ApplicationBundle\Controller;

use SC\BooksBundle\Entity\Book;
use SC\BooksBundle\Entity\BookFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class BooksController extends Controller
{


    public function showSingleAction(Request $request)
    {
        $bookId = (int)$request->get('book_id');

        if ($bookId == 0) {
            return new Response('Книга не найдена', 404);
        }

        $book = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBookById($bookId);

        if ((is_null($book)) || (!$book->getIsPublished())) {
            return new Response('Книга не найдена', 404);
        }


        return $this->render('DarulKutubApplicationBundle:Books:showSingle.html.twig', [
            'book' => $book
        ]);
    }

    public function downloadPDFBookAction(Request $request, BookFile $book)
    {
        $filename = realpath($this->get('kernel')->getRootDir() . '/../web/' . $book->getBookPDFPath());

        $response = new BinaryFileResponse($filename);

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $book->getBook()->getTitle() . '.pdf'
        );

        return $response;
    }

    public function downloadEPUBBookAction(Request $request, BookFile $book)
    {
        $filename =  realpath($this->get('kernel')->getRootDir() . '/../web/' . $book->getBookEPUBPath());

        $response = new BinaryFileResponse($filename);

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $book->getBook()->getTitle() . '.epub'
        );

        return $response;
    }

    public function downloadFB2BookAction(Request $request, BookFile $book)
    {
        $filename = $this->get('kernel')->getRootDir() . '/../web/' . $book->getBookFB2Path();

        $response = new BinaryFileResponse($filename);

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $book->getBook()->getTitle() . '.fb2'
        );

        return $response;
    }


    public function showAllAction(Request $request)
    {
        $parameters = $books = array();
        $parameters['isPublished'] = true;
        if (!is_null($request->get('type'))) {
            switch ($request->get('type')) {
                case 'popular':
                    $popularFiles = $this->getDoctrine()->getRepository('SCBooksBundle:BookFiles')->findBy(array(), array('downloadCnt' => 'desc'));
                    $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findByFiles($popularFiles, array());
                    $data['type'] = 'popular';
                    break;
                case 'recommended':
                    $parameters['isRecommended'] = true;
                    $data['type'] = 'recommended';
                    break;
                default:
                    $data['type'] = 'recent';
                    break;
            }
        }

        if (empty($books)) {
            $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBy($parameters, array('createdAt' => 'desc'));
        }

        foreach ($books as $book) {
            $coverUrl = '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId() . '/cover/' . $book->getId() . '.jpg';
            $book->setCoverUrl($coverUrl);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $books,
            $request->query->get('page', 1),
            $limit = 20
        );

        $data['pagination'] = $pagination;

        return $this->render('DarulKutubApplicationBundle:Books:showAll.html.twig', $data);
    }

    public function showBooksInCategoryAction(Request $request, $category_path)
    {
        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneByPath($category_path);

        $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBy(array('primary_category' => $category, 'isPublished' => true));

        if (sizeof($books) == 0) {
            $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBySecondaryCategories(array($category));
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $books,
            $request->query->get('page', 1),
            $limit = 20
        );

        foreach ($books as $book) {
            $coverUrl = '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId() . '/cover/' . $book->getId() . '.jpg';
            $book->setCoverUrl($coverUrl);
        }

        $data = array('pagination' => $pagination);

        return $this->render('DarulKutubApplicationBundle:Books:showInCategory.html.twig', $data);
    }

    public function showBooksInSubCategoryAction(Request $request, $category_path, $subcategory_path)
    {
        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneByPath($category_path);

        $subcategory = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneBy([
            'parentId' => $category->getId(),
            'path' => $subcategory_path
        ]);

        $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBySubcategory($subcategory);

        if (sizeof($books) == 0) {
            $books = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findBySecondaryCategories(array($category));
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $books,
            $request->query->get('page', 1),
            $limit = 20
        );

        foreach ($books as $book) {
            $coverUrl = '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId() . '/cover/' . $book->getId() . '.jpg';
            $book->setCoverUrl($coverUrl);
        }

        $data = array('pagination' => $pagination);

        return $this->render('DarulKutubApplicationBundle:Books:showInCategory.html.twig', $data);
    }


    public function downloadStatisticAction(Request $request)
    {
        $bookId = (int)$request->get('book_id');
        $fileId = (int)$request->get('file_id');

        $book = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findOneById($bookId);

        if (is_null($book)) {
            return new JsonResponse(array('code' => 'error', 'message' => 'Книга не найдена'));
        }

        foreach ($book->getFiles() as $file) {
            if ($file->getId() == $fileId) {
                $file->setDownloadCnt($file->getDownloadCnt() + 1);
                $this->getDoctrine()->getManager()->persist($file);
                $this->getDoctrine()->getManager()->flush();
            }
        }

        return new JsonResponse(array('code' => 'ok'));

    }

    public function downloadAction(Request $request, $file_id, $book_id)
    {
        $file = $this->getDoctrine()->getRepository('SCBooksBundle:BookFiles')->findOneById($file_id);

        if (is_null($file)) {
            return new Response('нет такого файла');
        }

        $book = $this->getDoctrine()->getRepository('SCBooksBundle:Book')->findOneById($book_id);

        if (is_null($book)) {
            return new Response('Книга не найдена');
        }

        $fileUrl = WEB_DIRECTORY . '/files/books/part_' . (floor($book->getId() / 10000)) . '/' . $book->getId() . '/' . $file->getFilename();

        $response = new Response();
        if (!is_file($fileUrl)) {
            $response->setStatusCode(500);
            return $response;
        };

        $finfo = new \finfo(FILEINFO_MIME);
        $fres = $finfo->file($fileUrl);

        $response->headers->set('Content-Type', $fres);
        //$response->headers->set('X-Accel-Redirect', $fileUrl);
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $file->getFilename());

        $response->sendHeaders();

        $response->setContent(file_get_contents($fileUrl));
        return $response;

    }
}
