<?php

namespace DarulKutub\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ServiceController extends Controller
{
    public function radioWidgetAction()
    {
        return $this->render('DarulKutubApplicationBundle:Service:radio_widget.html.twig');
    }
}