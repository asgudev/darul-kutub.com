$(".short-search").on('click', function () {
    if (window.innerWidth <= 480) $(".logo").toggle();
    $(this).toggleClass('active');
    $(".short-search input").focus();
});

$(".short-search input").on('click', function () {
    event.stopPropagation();
    return false;
});

function formatCategory(state) {
    if (!state.id) {
        return state.text;
    }
    return $(
        '<span>' + state.text + '</span>'
    );
};

$("body").on("click", function() {
    $(".hint-results").remove();
});

function renderSearchHint(context, results) {
    $(context).parent("div").find(".hint-results").remove();
    var $results = $("<ul />", {"class": "hint-results"});

    results.forEach(function (item) {
        var $item = $("<li />").text(item.name);
        $item.on("click", function () {
            $(context).val(item.name);
            $results.hide();
        });
        $results.append($item);
    });

    $(context).parent("div").append($results);

}

var inputTimeout = false;



$(document).on("keyup", ".short-search input", function () {

});

var xhr = false;
$(document).on("keyup", "#author, #title", function () {
    var type = $(this).attr("id"),
        author = $("#author").val(),
        title = $("#title").val(),
        context = this;

    if ($(this).val() == "") {
        $(".hint-results").remove();
        return false;
    }

    if (xhr)
        xhr.abort();

    clearTimeout(inputTimeout);

    inputTimeout = setTimeout(function () {
        xhr = $.ajax({
            url: "/search/",
            type: "POST",
            data: {type, author, title},
            context: context,
            success: function (result) {
                renderSearchHint(this, result);
            }
        });
    }, 250);

});