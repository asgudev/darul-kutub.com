(function ($) {
    $.fn.collection = function (options) {
        var settings = $.extend({
            addButtonText: 'Добавить',
            deleteButtonText: 'Удалить',
            items: [],
            uniqueItems: [],
            unique: false,
        }, options);


        var $collectionHolder = this;

        var $addItemLink = $('<a href="#" class="btn btn-success">' + settings.addButtonText + '</a>');
        var $newItemLi = $('<div />').append($addItemLink);

        $collectionHolder.append($newItemLi);

        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addItemLink.on('click', function (e) {
            e.preventDefault();
            addItemForm($collectionHolder, $newItemLi);
        });

        $collectionHolder.find('div.row').each(function () {
            addItemFormDeleteLink($(this));
        });

        function addItemForm() {
            var prototype = $collectionHolder.data('prototype');

            var index = $collectionHolder.data('index');

            var newForm = prototype.replace(/__name__/g, index);



            $collectionHolder.data('index', index + 1);


            var $newFormLi = $(newForm);
            $newItemLi.before($newFormLi);


            if (settings.unique) {
                settings.uniqueItems.push($newFormLi);

                settings.unique();
            }

            //specific
            //$newFormLi.find(".customPrice").val($("#category_discounts").val());

            addItemFormDeleteLink($newFormLi);
        }

        function addItemFormDeleteLink($itemFormLi) {
            var $removeFormA = $('<div class="col-md-2"><a href="#" class="btn btn-danger btn-block" style="margin-top: 25px;">' + settings.deleteButtonText + '</a></div>');
            $itemFormLi.append($removeFormA);

            $removeFormA.on('click', function (e) {
                e.preventDefault();
                $itemFormLi.remove();
            });
        }

        return this;
    };
}(jQuery));

(function ($) {
    $.fn.ajaxform = function (options) {
        var settings = $.extend({
            addButtonText: 'Добавить',
            deleteButtonText: 'Удалить',
            items: [],
            uniqueItems: [],
            unique: false,
        }, options);


        
    };
}(jQuery));