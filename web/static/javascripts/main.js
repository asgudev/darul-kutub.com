
$(function(){
    function centeringSubmenu(el){
        var parentWidth = $(el).outerWidth(),
            child = $(el).find('ul'),
            childWidth = child.outerWidth(),
            left = Math.abs(parentWidth / 2 - childWidth / 2),
            sign = parentWidth > childWidth ? 1 : -1
        child.css('left', sign * left);
    }

    $('.parent > li').each(function(){
        centeringSubmenu($(this));
    });

    $('.jcarousel').jcarousel();

    $('.book').bind('mouseenter', function(e){
        var $parent = $(this).parent(),
            $expandedDiv = $(this).closest(':has(.expand)').find('.expand'),
            shadowHeight = 3;
        $parent
            .addClass('hover')
            .height(shadowHeight + $expandedDiv.outerHeight(true));
    });
    $('.book').parent().bind('mouseleave', function(e){
        var $parent = $(this);
        $parent.removeClass('hover')
               .css('height', 'auto');
    });

    $('.scroll-top').click(function(e){
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 });
    });
});